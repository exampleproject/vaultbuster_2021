﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Playables;

public class TransferEffects : OpenDialog
{
    [Space(40)]
    public AudioSource Alarm;
    public TextMeshProUGUI CodeCrackedLabel;
    public RectTransform Level2;
    public TextMeshProUGUI DragLabel;
    public TextMeshProUGUI Code;
    public HandTip Hand;
    public RectTransform Level3;
    public Image FlashLight;
    public RectTransform Level4;
    public Image OpenVault;
    public AudioSource CoinsSFX;
    public ParticleSystem Coins;
    public RectTransform Level5;
    public AudioSource GlingAudio;
    public TextMeshProUGUI CompleteTransferLabel;

    private Coroutine _handTip;
    private string _dragLabelText;
    private string _opponentValutName;
    private string _secondWinLabelText;

    private void Awake()
    {
        _dragLabelText = DragLabel.text;
        _opponentValutName = UIReferences.References.BreakIn.OpponentVault.text;
        _secondWinLabelText = UIReferences.References.BreakIn.SecondWinLabel.text;
    }

    protected override void OnEnable()
    {
        UIReferences.References.BreakIn.OpponentVault.text = string.Format(_opponentValutName, AppManager.App.OpponentPlayer.Name);
        UIReferences.References.BreakIn.SecondWinLabel.text = string.Format(_secondWinLabelText, AppManager.App.OpponentPlayer.Name);
        string firstName = AppManager.App.OpponentPlayer.Name;
        if (AppManager.App.OpponentPlayer.Name.Contains("_"))
        {
            firstName = AppManager.App.OpponentPlayer.Name.Split('_')[0];
        }
        else if(AppManager.App.OpponentPlayer.Name.Contains(" "))
        {
            firstName = AppManager.App.OpponentPlayer.Name.Split()[0];
        }
        DragLabel.text = string.Format(_dragLabelText, firstName);

        base.OnEnable();

        //StartCoroutine(StartAnim());
    }

    private IEnumerator StartAnim()
    {
        Alarm.Play();
        yield return PlayAnimationCoroutine("BlinkLabel", BlinkLabelAction);
        yield return PlayAnimationCoroutine("LightLabel", BlinkLabelAction);
        Level2.gameObject.SetActive(true);
        yield return PlayAnimationCoroutine("BlinkLabel", BlinkCodeAction);
        yield return PlayAnimationCoroutine("LightLabel", BlinkCodeAction);
        var dragCode = Code.GetComponent<DragCode>();
        dragCode.enabled = true;
        _handTip = StartCoroutine(HandTip(dragCode));
        while (dragCode.enabled)
        {
            yield return null;
        }
        if (_handTip != null)
        {
            StopCoroutine(_handTip);
        }
        if (Hand.gameObject.activeInHierarchy)
        {
            Hand.gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(0.5f);
        Level3.gameObject.SetActive(true);
        CoinsSFX.Play();
        yield return PlayAnimationCoroutine("BlinkFlash", BlinkFlashAction);
        yield return PlayAnimationCoroutine("LightFlash", BlinkFlashAction);

        Level4.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        OpenVault.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        Coins.gameObject.SetActive(true);
        while (CoinsSFX.isPlaying)
        {
            yield return null;
        }
        Coins.gameObject.SetActive(false);
        Level5.gameObject.SetActive(true);
        GlingAudio.Play();
        yield return PlayAnimationCoroutine("BlinkCompleteLabel", BlinkCompleteAction);
        yield return PlayAnimationCoroutine("LightCompleteLabel", BlinkCompleteAction);
        yield return new WaitForSeconds(1);
        UIReferences.References.BreakIn.WinSum.text = FuncitionsManager.Functions.WinCoins.ToString();
        UIReferences.References.ActiveScreen(ScreensManager.Screens.REWARD, ScreensManager.Requires.NONE);
    }

    private IEnumerator HandTip(DragCode dragCode)
    {
        for (var i = 0; i < 5 || !dragCode.enabled; i++)
        {
            yield return new WaitForSeconds(3);
            Hand.gameObject.SetActive(true);
            while (Hand.gameObject.activeInHierarchy)
            {
                yield return null;
            }
        }
    }

    private void BlinkLabelAction(GameObject animatedObject, float percent)
    {
        CodeCrackedLabel.color = new Color(CodeCrackedLabel.color.r, CodeCrackedLabel.color.g, CodeCrackedLabel.color.b, percent);
    }

    private void BlinkCodeAction(GameObject animatedObject, float percent)
    {
        Code.color = new Color(Code.color.r, Code.color.g, Code.color.b, percent);
    }

    private void BlinkFlashAction(GameObject animatedObject, float percent)
    {
        FlashLight.color = new Color(FlashLight.color.r, FlashLight.color.g, FlashLight.color.b, percent);
    }

    private void BlinkCompleteAction(GameObject animatedObject, float percent)
    {
        CompleteTransferLabel.color = new Color(CompleteTransferLabel.color.r, CompleteTransferLabel.color.g, CompleteTransferLabel.color.b, percent);
    }

    public void Close()
    {
        UIReferences.References.ActiveScreen(ScreensManager.Screens.REWARD, ScreensManager.Requires.NONE);
    }

    private void OnDisable()
    {
        //transform.localScale = Vector3.one;
        //CodeCrackedLabel.color = new Color(CodeCrackedLabel.color.r, CodeCrackedLabel.color.g, CodeCrackedLabel.color.b, 0);
        //Level2.gameObject.SetActive(false);
        //Code.color = new Color(Code.color.r, Code.color.g, Code.color.b, 0);
        //Hand.gameObject.SetActive(false);
        //Level3.gameObject.SetActive(false);
        //FlashLight.color = new Color(FlashLight.color.r, FlashLight.color.g, FlashLight.color.b, 0);
        //Level4.gameObject.SetActive(false);
        //OpenVault.gameObject.SetActive(false);
        //Coins.gameObject.SetActive(false);
        //Level5.gameObject.SetActive(false);
        //CompleteTransferLabel.color = new Color(CompleteTransferLabel.color.r, CompleteTransferLabel.color.g, CompleteTransferLabel.color.b, 0);

        foreach(var timeline in GetComponentsInChildren<PlayableDirector>())
        {
            timeline.Stop();
        }

        AppManager.App.CurrentUser.Vault.Coins += FuncitionsManager.Functions.WinCoins;
        UIReferences.References.TopBar.CoinsCounter.text = AppManager.App.CurrentUser.Vault.Coins.ToString();
        UIReferences.References.GameFllow.ChoosePlayerScreen.GetComponent<ChoosePlayer>().OnRefereshClick(false);
    }
}
