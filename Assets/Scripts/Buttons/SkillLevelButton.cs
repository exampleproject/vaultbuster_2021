﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillLevelButton : MonoBehaviour
{
    public int Min;
    public int Max;

    private Button _btn;
    private Image _currentLevel;
    private Image _lock;
    private Image _selectedFrame;

    private void Awake()
    {
        _btn = GetComponent<Button>();
        _currentLevel = transform.Find("CurrentLevel").GetComponent<Image>();
        _lock = transform.Find("Lock").GetComponent<Image>();
        _selectedFrame = transform.Find("SelectedFrame").GetComponent<Image>();

        _currentLevel.gameObject.SetActive(false);
        _lock.gameObject.SetActive(false);
        _selectedFrame.gameObject.SetActive(false);
    }

    public void SetButton()
    {
        if (GameProgressConfig.GameProgress == null || AppManager.App == null || AppManager.App.CurrentUser == null)
        {
            _btn.interactable = false;
            return;
        }
        _currentLevel.gameObject.SetActive(GameProgressConfig.GameProgress.GetCurrentXPData().UpTo == Max);
        _btn.interactable = GameProgressConfig.GameProgress.GetCurrentXPData().UpTo >= Max;
        _lock.gameObject.SetActive(!_btn.interactable);
    }

    public void OnSkillLevelClick()
    {
        AppManager.App.CurrentMinToBust = Min;
        AppManager.App.CurrentMaxToBust = Max;
        if (Max < GameProgressConfig.GameProgress.GetCurrentXPData().UpTo)
        {
            GameProgressConfig.GameProgress.SetChoosenIndexByUpTo(Max);
        }
        else
        {
            GameProgressConfig.GameProgress.SetChoosenIndex(AppManager.App.CurrentUser.XP);
        }
        _selectedFrame.gameObject.SetActive(true);
        StartCoroutine(WaitAfterClick());
    }

    private IEnumerator WaitAfterClick()
    {
        yield return new WaitForSeconds(0.7f);
        UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_PLAYER, ScreensManager.Requires.NONE);
    }

    private void OnEnable()
    {
        if (_selectedFrame.gameObject.activeInHierarchy)
        {
            _selectedFrame.gameObject.SetActive(false);
        }
    }
}
