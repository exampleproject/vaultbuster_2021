﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BeakInBoosterButton : MonoBehaviour
{
    public int CoolDownTime = 30;

    private Button _btn;

    private void Start()
    {
        _btn = GetComponent<Button>();
    }

    public void OnBoosterClick()
    {
        StartCoroutine(CoolDown());
    }

    IEnumerator CoolDown()
    {
        _btn.interactable = false;
        yield return new WaitForSeconds(CoolDownTime);
        _btn.interactable = true;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        _btn.interactable = true;
    }
}
