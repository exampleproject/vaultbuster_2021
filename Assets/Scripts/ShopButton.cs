﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButton : MonoBehaviour
{
    public AudioSource ClickSound { get; set; }
    public int Sum { get; set; }
    public int Price { get; set; }

    private Dictionary<string, object> _updateValues = new Dictionary<string, object>();
    private string _keyPath;
    private string _goldKeyPath;

    public DatabaseManager.UserDataKeys Key
    {
        get
        {
            return _key;
        }
        set
        {
            _key = value;
            _keyPath = DatabaseManager.Database.GetPathFromKey(_key);
            _goldKeyPath = DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.GoldBars);
            if (_updateValues.ContainsKey(_keyPath))
            {
                _updateValues[_keyPath] = (int)DatabaseManager.Database.GetObjectByKey(_key);
            }
            else
            {
                _updateValues.Add(_keyPath, (int)DatabaseManager.Database.GetObjectByKey(_key));
            }
            if (_updateValues.ContainsKey(_goldKeyPath))
            {
                _updateValues[_goldKeyPath] = AppManager.App.CurrentUser.Vault.GoldBars;
            }
            else
            {
                _updateValues.Add(_goldKeyPath, AppManager.App.CurrentUser.Vault.GoldBars);
            }
        }
    }
    private DatabaseManager.UserDataKeys _key;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnBuyClick);
    }

    private void OnBuyClick()
    {
        ClickSound.Play();
        _updateValues[_keyPath] = (int)_updateValues[_keyPath] + Sum;
        _updateValues[_goldKeyPath] = (int)_updateValues[_goldKeyPath] - Price;
        DatabaseManager.Database.UpdateUserDataPrameters(_updateValues);
    }
}
