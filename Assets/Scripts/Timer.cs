﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public int Seconds;
    public TMPro.TextMeshProUGUI TimerText;
    public Image TimerBar;
    public UnityEvent TimeIsUp;
    private int _currentTime;
    private Coroutine _countTime;
    private bool _isPause;

    public void StartTimer()
    {
        _isPause = false;
        _currentTime = Seconds;
        _countTime = StartCoroutine(CountTime());
    }

    public void PauseTimer()
    {
        _isPause = true;
    }

    public void ContinueTimer()
    {
        _isPause = false;
    }

    IEnumerator CountTime()
    {
        while (_currentTime > 0)
        {
            if (!_isPause)
            {
                TimerText.text = _currentTime.ToString();
                TimerBar.fillAmount = (float)_currentTime / Seconds;
                _currentTime--;
                yield return new WaitForSecondsRealtime(1);
            }
            else
            {
                yield return null;
            }
        }
        TimerText.text = 0.ToString();
        TimerBar.fillAmount = 0;
        TimeIsUp.Invoke();
    }

    public void AddTime(int timeToAdd)
    {
        _currentTime = Mathf.Min(Seconds, _currentTime + timeToAdd);
    }

    public void SubtractTime(int timeToSubtract)
    {
        _currentTime = Mathf.Max(0, _currentTime - timeToSubtract);
    }

    public void StopTimer()
    {
        StopCoroutine(_countTime);
    }

    public int GetCurrentTime()
    {
        return _currentTime;
    }
}
