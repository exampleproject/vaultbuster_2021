﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandTip : AnimationCode
{
    [Space(40)]
    public Vector2 StartPosition;
    public Vector2 EndPosition;

    private void OnEnable()
    {
        StartCoroutine(HandTipAnimation());
    }

    IEnumerator HandTipAnimation()
    {
        yield return PlayAnimationCoroutine("HandTip", HandTipAction);
        gameObject.SetActive(false);
    }

    private void HandTipAction(GameObject animatedObject, float percent)
    {
        GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(StartPosition, EndPosition, percent);
    }
}
