﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldDialog : AnimationCode
{
    [Space(20)]
    public AudioSource ShieldAudio;
    public Image ShieldImage;
    public Vector2 StartAnchor;
    public Vector2 EndAnchor;

    private Vector2 _lowestAnchor;

    private void Awake()
    {
        _lowestAnchor = EndAnchor + ((EndAnchor - StartAnchor) * 0.1f);
    }

    private void OnEnable()
    {
        transform.localScale = Vector3.zero;
        StartCoroutine(OpenShieldDialog());
    }

    private IEnumerator OpenShieldDialog()
    {
        ShieldAudio.Play();
        PlayAnimation("Scale", Scaleup);
        yield return new WaitForSeconds(0.25f);
        yield return PlayAnimationCoroutine("ShieldDown", ShieldDown);
        yield return PlayAnimationCoroutine("ShieldUp", ShieldUp);
    }

    public void OnClose()
    {
        StartCoroutine(Close());
    }

    private IEnumerator Close()
    {
        yield return PlayAnimationCoroutine("Scale", ScaleDown);
        gameObject.SetActive(false);
    }

    private void Scaleup(GameObject animatedObject, float percent)
    {
        transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, percent);
    }

    private void ScaleDown(GameObject animatedObject, float percent)
    {
        transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, percent);
    }

    private void ShieldDown(GameObject animatedObject, float percent)
    {
        ShieldImage.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(StartAnchor, _lowestAnchor, percent);
    }

    private void ShieldUp(GameObject animatedObject, float percent)
    {
        ShieldImage.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(_lowestAnchor, EndAnchor, percent);
    }
}
