﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class OpenDialog : AnimationCode
{
    public bool IsFadeIn = true;

    public void Reset()
    {
        Animations = new List<AnimationParameters>
        {
            new AnimationParameters
            {
                AnimationName = "Fade",
                AnimationTime = 1,
                Repeat = 1,
                PercentsCurve = new AnimationCurve(new Keyframe[]{ new Keyframe(0, 0, 1, 1), new Keyframe(1, 1, 1, 1) })
            }
        };
    }

    protected virtual void OnEnable()
    {
        Image[] images = GetComponentsInChildren<Image>();
        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>();

        if (IsFadeIn)
        {
            //transform.localScale = Vector3.zero;
            //PlayAnimation("Scale", Scaleup);

            foreach (var image in images)
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
                PlayAnimation("Fade", FadeIn, image.gameObject);
            }
            foreach (var text in texts)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
                PlayAnimation("Fade", FadeIn, text.gameObject);
            }
        }
        else
        {
            foreach (var image in images)
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
            }
            foreach (var text in texts)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
            }
        }
    }

    public void OnClose()
    {
        StartCoroutine(Close());
    }

    private IEnumerator Close()
    {
        Image[] images = GetComponentsInChildren<Image>();
        TextMeshProUGUI[] texts = GetComponentsInChildren<TextMeshProUGUI>();

        foreach (var image in images)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
            PlayAnimation("Fade", FadeOut, image.gameObject);
        }
        foreach (var text in texts)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
            PlayAnimation("Fade", FadeOut, text.gameObject);
        }
        //yield return PlayAnimationCoroutine("Scale", ScaleDown);
        yield return new WaitForSeconds(Animations.Where(a => a.AnimationName == "Fade").Select(a => a.AnimationTime).FirstOrDefault());
        gameObject.SetActive(false);
        foreach (var image in images)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
        }
        foreach (var text in texts)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        }
    }

    //private void Scaleup(GameObject animatedObject, float percent)
    //{
    //    transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, percent);
    //}

    private void FadeIn(GameObject animatedObject, float percent)
    {
        //transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, percent);
        var image = animatedObject.GetComponent<Image>();
        if (image != null)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, percent);
            return;
        }
        var text = animatedObject.GetComponent<TextMeshProUGUI>();
        if (text != null)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, percent);
        }
    }

    //private void ScaleDown(GameObject animatedObject, float percent)
    //{
    //    transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, percent);
    //}

    private void FadeOut(GameObject animatedObject, float percent)
    {
        //transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, percent);
        var image = animatedObject.GetComponent<Image>();
        if (image != null)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1 - percent);
            return;
        }
        var text = animatedObject.GetComponent<TextMeshProUGUI>();
        if (text != null)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1 - percent);
        }
    }
}
