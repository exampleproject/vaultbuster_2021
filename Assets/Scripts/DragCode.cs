﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;

public class DragCode : AnimationCode, IDragHandler, IEndDragHandler
{
    public Vector2 EndPoint = new Vector2(-145, -270);
    public PlayableDirector TransferCoinsEndTimeline;
    public HandTip Hand;

    private RectTransform _rectTransform;
    private Vector3 _startPos;

    private void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _startPos = _rectTransform.anchoredPosition;
    }

    private void OnEnable()
    {
        StartCoroutine(HandTip());
    }

    public void OnDrag(PointerEventData eventData)
    {
#if UNITY_EDITOR
        var screenPoint = Input.mousePosition;
        screenPoint.z = 100.0f; //distance of the plane from the camera
        transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
#elif UNITY_ANDROID
        var screenPoint = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 100f);
        transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
#endif
        if (_rectTransform.anchoredPosition.x < -140)
        {
            _rectTransform.anchoredPosition = new Vector2(-140, _rectTransform.anchoredPosition.y);
        }
        if (_rectTransform.anchoredPosition.x > 140)
        {
            _rectTransform.anchoredPosition = new Vector2(140, _rectTransform.anchoredPosition.y);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        StartCoroutine(EndDargPoint());
    }

    private IEnumerator EndDargPoint()
    {
        var rectTransform = GetComponent<RectTransform>();
        if (rectTransform.anchoredPosition.x < 0 && rectTransform.anchoredPosition.y < -90)
        {
            this.enabled = false;
            yield return PlayAnimationCoroutine("GoToPoint", GoToEndPointAction);
            yield return new WaitForSeconds(0.25f);
            _rectTransform.anchoredPosition = _startPos;
            TransferCoinsEndTimeline.Play();
            gameObject.SetActive(false);
        }
        else
        {
            PlayAnimation("GoToPoint", GoToStartPointAction);
        }
    }

    private void GoToEndPointAction(GameObject animatedObject, float percent)
    {
        _rectTransform.anchoredPosition = Vector2.Lerp(_rectTransform.anchoredPosition, EndPoint, percent);
    }

    private void GoToStartPointAction(GameObject animatedObject, float percent)
    {
        _rectTransform.anchoredPosition = Vector2.Lerp(_rectTransform.anchoredPosition, _startPos, percent);
    }

    private IEnumerator HandTip()
    {
        for (var i = 0; i < 5 || !enabled; i++)
        {
            yield return new WaitForSeconds(3);
            Hand.gameObject.SetActive(true);
            while (Hand.gameObject.activeInHierarchy)
            {
                yield return null;
            }
        }
        Hand.gameObject.SetActive(false);
    }
}
