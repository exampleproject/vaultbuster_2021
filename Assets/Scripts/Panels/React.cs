﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class React : AnimationCode
{
    public bool IsSuccess;
    public float PushButtonEffectTime = 3;
    public Sprite PushSprite;
    public Sprite WaitSrpite_1;
    public Sprite WaitWprite_2;
    public Sprite DoneSprite;
    public Sprite VSprite;
    public Sprite FailSprite;
    public List<Sprite> PushButtonImagesDone;
    public List<Sprite> PushButtonImagesFaild;
    public Color StartLabelColor;
    public Color EndLabelColor;
    public Color CodeChangedColor;

    public string StartLabelText = "HURRY !!! CHANGE\nYOUR CODE, {0} IS\nBUSTING YOUR VAULT!";
    public string WaitLabelText = "PLEASE WAIT A WHILE";
    public string CodeChangedText = "CODE SUCCESSFULLY\nCHANGED";
    public string FailText = "SORRY, TOO LATE,\n{0} STOLE {1}\nGOLD COINS FROM YOU";

    public Button PushButton;
    public Button GameButton;
    public TextMeshProUGUI HeaderLabel;
    public TextMeshProUGUI SeconderyLabel;
    public TextMeshProUGUI SeconderyFailLabel;

    private Image _pushImage;

    private void Awake()
    {
        _pushImage = PushButton.GetComponent<Image>();
    }

    private void Start()
    {
        DatabaseManager.Database.IsReactUnderAttack.AddListener(OnIsReactUnderAttack);
    }

    private void OnIsReactUnderAttack(bool isUnderAttack)
    {
        IsSuccess = isUnderAttack;
    }

    private void OnEnable()
    {
        SeconderyLabel.gameObject.SetActive(true);
        HeaderLabel.text = string.Format(StartLabelText, AppManager.App.AttackBuster.SenderName);
        _pushImage.sprite = PushSprite;
        _pushImage.gameObject.SetActive(true);
        GameButton.gameObject.SetActive(false);
        SeconderyFailLabel.gameObject.SetActive(false);
    }

    public void OnPushClick()
    {
        DatabaseManager.Database.CheckIsUnderAttack();
        if (!AppManager.App.AttackBuster.IsBot)
        {
            FuncitionsManager.Functions.changeCode();
        }
        StartCoroutine(Push());
    }

    private IEnumerator Push()
    {
        SeconderyLabel.gameObject.SetActive(false);
        HeaderLabel.text = WaitLabelText;
        yield return PlayAnimationCoroutine("ChangeHeaderColor", ChangeLabelColor);
        HeaderLabel.gameObject.SetActive(false);
        if (IsSuccess)
        {
            _pushImage.sprite = DoneSprite;
            yield return new WaitForSeconds(1);
            HeaderLabel.text = CodeChangedText;
            HeaderLabel.color = CodeChangedColor;
            HeaderLabel.gameObject.SetActive(true);
            _pushImage.sprite = VSprite;
        }
        else
        {
            _pushImage.sprite = FailSprite;
            yield return new WaitForSeconds(1);
            //TODO
            HeaderLabel.text = string.Format(FailText, AppManager.App.AttackBuster.SenderName, "");
            HeaderLabel.color = StartLabelColor;
            HeaderLabel.gameObject.SetActive(true);
            _pushImage.gameObject.SetActive(false);
            SeconderyFailLabel.gameObject.SetActive(true);
        }
        GameButton.gameObject.SetActive(true);
    }

    private void ChangeLabelColor(GameObject animatedObject, float percent)
    {
        HeaderLabel.color = Color.Lerp(StartLabelColor, EndLabelColor, percent);
        if(percent > 0.5f)
        {
            _pushImage.sprite = WaitWprite_2;
        }
        else
        {
            _pushImage.sprite = WaitSrpite_1;
        }
    }
}
