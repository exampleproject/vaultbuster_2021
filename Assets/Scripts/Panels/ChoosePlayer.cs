﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class ChoosePlayer : MonoBehaviour
{
    public int BotsPercents = 75;
    public TextMeshProUGUI PlayerName;
    public Image PlayerImage;
    public Image PlayerTrophy;
    public TextMeshProUGUI UpToValue;
    public GameObject RevengeObject;
    public AudioSource ClickSound;
    public TextMeshProUGUI FriendName;
    public Image FriendImage;
    public Image FriendTrophy;
    public TextMeshProUGUI FriendUpToValue;

    private List<PlayerData> _playersOpponents = new List<PlayerData>();
    private List<PlayerData> _botsOpponents = new List<PlayerData>();
    private List<PlayerData> _friends = new List<PlayerData>();
    private bool _isGotPlayers;
    private bool _isGotBots;
    private bool _isGotFriends;
    private bool _isRandomPlayer;
    private PlayerData _player;
    private Sprite[] _playersImages;

    private void Awake()
    {
        FuncitionsManager.Functions.GotPlayersOpponents.AddListener(OnGotPlayers);
        FuncitionsManager.Functions.GotBotsOpponents.AddListener(OnGotBots);
        FuncitionsManager.Functions.GotFriends.AddListener(OnGotFriends);
        _playersImages = Resources.LoadAll<Sprite>("Sprites/avatars");
    }

    public void GetOpponents(int min, int max)
    {
        if (_isRandomPlayer)
        {
            var upTo = int.Parse(UpToValue.text, NumberStyles.AllowThousands);
            if (upTo == GameProgressConfig.GameProgress.GetChoosenXPData().UpTo)
            {
                SetRevenge();
                ShowScreen();
                return;
            }
        }
        FuncitionsManager.Functions.GetPlayersOpponents(min, max);
        FuncitionsManager.Functions.GetBotsOpponents();
        FuncitionsManager.Functions.GetFriends();
        StartCoroutine(WaitForOpponents());
    }

    IEnumerator WaitForOpponents()
    {
        while (!(_isGotPlayers && _isGotBots && _isGotFriends))
        {
            yield return null;
        }
        SetPlayer();
        SetRevenge();
        SetFriend();
    }

    private void OnGotPlayers(Players players)
    {
        _playersOpponents = players == null ? _playersOpponents = new List<PlayerData>() : players.PlayersData;
        var currentUser = _playersOpponents.Where(o => o.Uid == AppManager.App.CurrentUser.Id).FirstOrDefault();
        if (currentUser != null)
        {
            _playersOpponents.Remove(currentUser);
        }
        _isGotPlayers = true;
    }

    private void OnGotBots(Players bots)
    {
        _botsOpponents = bots == null ? new List<PlayerData>() : bots.PlayersData;
        foreach (var bot in _botsOpponents)
        {
            bot.IsBot = true;
        }
        _isGotBots = true;
    }

    private void OnGotFriends(Players friends)
    {
        _friends = friends == null ? new List<PlayerData>() : friends.PlayersData;
        _isGotFriends = true;
    }

    public void SetPlayer()
    {
        _player = null;
        while (_player == null)
        {
            if (Random.Range(0, 100) < BotsPercents || _playersOpponents.Count == 0)
            {
                Debug.Log(_botsOpponents.Count);
                _player = _botsOpponents[Random.Range(0, _botsOpponents.Count)];
                _player.IsBot = true;
                Debug.Log(_player);
            }
            else if (_playersOpponents.Count > 0)
            {
                _player = _playersOpponents[Random.Range(0, _playersOpponents.Count)];
            }
        }
        PlayerName.text = _player.Name;
        PlayerTrophy.sprite = GetPlayerTrophy(_player);
        UpToValue.text = string.Format("{0:n0}", GameProgressConfig.GameProgress.GetChoosenXPData().UpTo);
        if (_player.AvatarIndex == -1)
        {
            Debug.Log(_player.ImageUrl);
            StorageManager.Storage.SpriteDownloaded.AddListener(OnSpriteDownloaded);
            StorageManager.Storage.DownloadPlayerImage(_player.ImageUrl);
        }
        else
        {
            PlayerImage.sprite = _playersImages[Convert.ToInt32(_player.AvatarIndex)];
            UIReferences.References.BreakIn.OpponentImage.sprite = PlayerImage.sprite;
        }
        _isRandomPlayer = true;
        ShowScreen();
    }

    private void ShowScreen()
    {
        if (UIReferences.References.GameFllow.WaitWheel.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(false);
        }
        UIReferences.References.GameFllow.ChoosePlayerBackground.gameObject.SetActive(true);
        var tabs = GetComponentInChildren<ToggleGroup>();
        tabs.GetComponentsInChildren<Toggle>()[0].isOn = true;
    }

    private void OnSpriteDownloaded(Sprite sprite)
    {
        PlayerImage.sprite = sprite;
        UIReferences.References.BreakIn.OpponentImage.sprite = sprite;
        StorageManager.Storage.SpriteDownloaded.RemoveListener(OnSpriteDownloaded);
        if (UIReferences.References.GameFllow.WaitWheel.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(false);
        }
    }

    public void OnClick()
    {
        AppManager.App.OpponentPlayer = _player;
        UIReferences.References.BreakIn.OpponentTrophy.sprite = PlayerTrophy.sprite;
        if (!_player.IsBot)
        {
            FuncitionsManager.Functions.SendAttackAlarm(_player.Uid, AppManager.App.CurrentUser.Id, AppManager.App.CurrentUser.Name);
        }
        gameObject.SetActive(false);
        UIReferences.References.ActiveScreen(ScreensManager.Screens.BREAKIN, ScreensManager.Requires.NONE);
    }

    public void OnRefereshClick(bool isPay)
    {
        if (isPay)
        {
            AppManager.App.CurrentUser.Vault.Coins -= 10;
            FuncitionsManager.Functions.UpdateVault(FuncitionsManager.DataKeys.Coins, (AppManager.App.CurrentUser.Vault.Coins).ToString());
            UIReferences.References.TopBar.TopBarCanvas.GetComponent<TopbarManager>().UpdateTopBar();
        }
        _isRandomPlayer = false;
        SetPlayer();
    }

    public void SetRevenge()
    {
        if (AppManager.App.CurrentUser.RevengesList.Count == 0)
        {
            UIReferences.References.GameFllow.NoRevengeScreen.gameObject.SetActive(true);
            UIReferences.References.GameFllow.RevengeScreen.gameObject.SetActive(false);
            return;
        }

        UIReferences.References.GameFllow.NoRevengeScreen.gameObject.SetActive(false);
        UIReferences.References.GameFllow.RevengeScreen.gameObject.SetActive(true);

        foreach (var revenge in AppManager.App.CurrentUser.RevengesList)
        {
            if (revenge.Attacker != null)
            {
                var obj = Instantiate(RevengeObject, UIReferences.References.GameFllow.RevengeContainer.transform);
                obj.transform.Find("AttackerName").GetComponent<TextMeshProUGUI>().text = revenge.Attacker.Name;
                obj.transform.Find("StolenSum").GetComponent<TextMeshProUGUI>().text = revenge.StolenSum + "M";
                if (revenge.Attacker.AvatarIndex == -1)
                {
                    StorageManager.Storage.SpriteDownloaded.AddListener(OnSpriteDownloaded);
                    StorageManager.Storage.DownloadPlayerImage(revenge.Attacker.ImageUrl);
                }
                else
                {
                    Debug.Log(revenge.Attacker.AvatarIndex);
                    obj.transform.Find("Mask").GetChild(0).GetComponent<Image>().sprite = _playersImages[Convert.ToInt32(revenge.Attacker.AvatarIndex)];
                }
                //RevengeBustButton
                var bustBtn = obj.transform.Find("RevengeBustButton").GetComponent<Button>();
                bustBtn.onClick.AddListener(() => { OnRevengeBustClick(revenge, obj.transform.Find("Mask").GetChild(0).GetComponent<Image>().sprite); });
            }
        }
    }

    private void OnRevengeBustClick(Revenge revenge, Sprite playerImage)
    {
        _player = revenge.Attacker;
        UIReferences.References.BreakIn.OpponentImage.sprite = playerImage;
        ClickSound.Play();
        OnClick();
        UIReferences.References.BreakIn.OpponentTrophy.sprite = GetPlayerTrophy(revenge.Attacker);
        AppManager.App.RemoveRevenge(revenge);
    }

    public void SetFriend()
    {
        if (_friends == null || _friends.Count == 0)
        {
            UIReferences.References.GameFllow.FriendsScreen.gameObject.SetActive(false);
            UIReferences.References.GameFllow.NoFriendsScreen.gameObject.SetActive(true);
        }
        else
        {
            UIReferences.References.GameFllow.FriendsScreen.gameObject.SetActive(true);
            UIReferences.References.GameFllow.NoFriendsScreen.gameObject.SetActive(false);

            var friend = _friends[Random.Range(0, _friends.Count)];

            FriendName.text = friend.Name;
            PlayerTrophy.sprite = GetPlayerTrophy(friend);
            UpToValue.text = string.Format("{0:n0}", GameProgressConfig.GameProgress.GetChoosenXPData().UpTo);
            if (_player.AvatarIndex == -1)
            {
                Debug.Log(friend.ImageUrl);
                StorageManager.Storage.SpriteDownloaded.AddListener(OnSpriteDownloaded);
                StorageManager.Storage.DownloadPlayerImage(friend.ImageUrl);
            }
            else
            {
                FriendImage.sprite = _playersImages[Convert.ToInt32(friend.AvatarIndex)];
                UIReferences.References.BreakIn.OpponentImage.sprite = FriendImage.sprite;
            }
            //_isRandomPlayer = true;
        }
    }

    private Sprite GetPlayerTrophy(PlayerData player)
    {
        if (player.IsBot)
        {
            var degree = GameProgressConfig.GameProgress.GetCurrentXPData().Degree;
            var level = Trophies.TrophiesSprites.GetLevelByDegree(degree);
            level += Random.Range(-1, 1);
            level = Mathf.Max(0, level);
            return Trophies.TrophiesSprites.GetTrophyByDegree(Trophies.TrophiesSprites.GetDegreeByLevel(level));
        }
        else if (!string.IsNullOrEmpty(player.Degree))
        {
            return Trophies.TrophiesSprites.GetTrophyByDegree(player.Degree);
        }
        return PlayerTrophy.sprite;
    }

    private void OnDisable()
    {
        _isGotPlayers = false;
        _isGotBots = false;
    }
}
