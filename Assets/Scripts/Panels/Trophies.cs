﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Trophies : MonoBehaviour
{
    private enum Degrees
    {
        beginner_buster = 0,
        advanced_beginner_buster = 1,
        skilled_beginner_buster = 2,
        talented_beginner_busterr = 3,
        advanced_buster = 4,
        skilled_advanced_buster = 5,
        talented_advanced_buster = 6,
        pro_buster = 7,
        advanced_pro_buster = 8,
        skilled_pro_buster = 9,
        talented_pro_buster = 10,
        senior_buster = 11,
        advanced_senior_buster = 12,
        skilled_senior_buster = 13,
        talented_senior_buster = 14,
        master_buster = 15,
        advanced_master_buster = 16,
        skilled_master_buster = 17,
        talented_master_buster = 18,
        super_master_buster = 19,
        advanced_super_master_buster = 20,
        skilled_super_master_buster = 21,
        talented_super_master_buster = 22,
        grand_master_buster = 23,
        advanced_grand_master_buster = 24,
        skilled_grand_master_buster = 25,
        talented_grand_master_buster = 26
    }

    public string ThropiesTexture = "Sprites/trophies-ONLY";

    private Sprite[] _trophiesImages;
    public static Trophies TrophiesSprites;

    private void Awake()
    {
        if(TrophiesSprites != null)
        {
            Destroy(gameObject);
            return;
        }
        TrophiesSprites = this;
        _trophiesImages = Resources.LoadAll<Sprite>(ThropiesTexture);
    }

    public int GetLevelByDegree(string degree)
    {
        degree = string.Join("_", degree.Split());
        Degrees d;
        if (!Enum.TryParse(degree, out d))
        {
            d = Degrees.advanced_beginner_buster;
        }

        return (int)d;
    }

    public Sprite GetTrophyByDegree(string degree)
    {
        var level = GetLevelByDegree(degree);
        return _trophiesImages[level];
    }

    public List<Sprite> GetPrevTrophiesByDegree(string degree)
    {
        var trophiesSprites = new List<Sprite>();
        var level = GetLevelByDegree(degree);
        for (var i = 0; i < level; i++)
        {
            trophiesSprites.Add(_trophiesImages[i]);
        }
        return trophiesSprites;
    }

    public string GetDegreeByLevel(int level)
    {
        var degree = ((Degrees)level).ToString();
        return (string.Join(" ", degree.Split('_')));
    }

    public bool IsLoaded()
    {
        return _trophiesImages != null && _trophiesImages.Length > 0;
    }
}
