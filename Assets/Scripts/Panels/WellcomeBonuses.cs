﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WellcomeBonuses : MonoBehaviour
{
    public TMPro.TextMeshProUGUI GamesSumText;
    public TMPro.TextMeshProUGUI GoldBarsSumText;
    public TMPro.TextMeshProUGUI BoostersSumText;
    public TMPro.TextMeshProUGUI ShieldsSumText;
    public TMPro.TextMeshProUGUI CoinsSumText;
    public int GamesSum = 10;
    public int GoldBarsSum = 5;
    public int BoostersSum = 10;
    public int ShieldsSum = 5;
    public int CoinsSum = 500;
    public float AnimationTime = 1;
    public AudioSource CollectSound;

    private void OnEnable()
    {
        transform.localScale = Vector3.one;
    }

    private void Start()
    {
        GamesSumText.text = GamesSum.ToString();
        GoldBarsSumText.text = GoldBarsSum.ToString();
        BoostersSumText.text = BoostersSum.ToString();
        ShieldsSumText.text = ShieldsSum.ToString();
        CoinsSumText.text = CoinsSum.ToString();
    }

    public void OnCollectClick()
    {
        var topBar = UIReferences.References.TopBar.TopBarCanvas.GetComponent<TopbarManager>();
        topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> {
            { DatabaseManager.UserDataKeys.Games, GamesSum },
            { DatabaseManager.UserDataKeys.GoldBars, GoldBarsSum },
            { DatabaseManager.UserDataKeys.Boosters, BoostersSum },
            { DatabaseManager.UserDataKeys.Shields,  ShieldsSum },
            { DatabaseManager.UserDataKeys.Coins, CoinsSum }
        });

        UIReferences.References.TopBar.WinGames.gameObject.SetActive(true);
        UIReferences.References.TopBar.WinGoldBars.gameObject.SetActive(true);
        UIReferences.References.TopBar.WinBoosters.gameObject.SetActive(true);
        UIReferences.References.TopBar.WinShields.gameObject.SetActive(true);
        UIReferences.References.TopBar.WinCoins.gameObject.SetActive(true);

        CollectSound.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        CollectSound.gameObject.SetActive(false);
    }
}
