﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnimationCode : MonoBehaviour
{
    public List<AnimationParameters> Animations;

    protected void PlayAnimation(string animationName, Action<GameObject, float> animationAction, GameObject animatedObject = null)
    {
        StartCoroutine(PlayAnimationCoroutine(animationName, animationAction, animatedObject));
    }

    protected IEnumerator PlayAnimationCoroutine(string animationName, Action<GameObject, float> animationAction, GameObject animatedObject = null)
    {
        var animationParams = Animations.Where(a => a.AnimationName == animationName).FirstOrDefault();
        if (animationParams != null)
        {
            var repeatTime = animationParams.AnimationTime / animationParams.Repeat;
            for (var i = 0; i < animationParams.Repeat; i++)
            {
                //var time = repeatTime * 0.5f;
                for (float timer = 0; timer < repeatTime; timer += Time.deltaTime)
                {
                    var curvePercent = animationParams.PercentsCurve.Evaluate(timer / repeatTime);
                    animationAction(animatedObject, curvePercent);
                    yield return null;
                }
                animationAction(animatedObject, animationParams.PercentsCurve.Evaluate(1));
            }
        }
    }
}

[Serializable]
public class AnimationParameters
{
    public string AnimationName;
    [Min(1)]
    public int Repeat;
    public float AnimationTime;
    public AnimationCurve PercentsCurve;
}