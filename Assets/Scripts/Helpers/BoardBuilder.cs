﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public enum CodeShapes
{
    Right,
    Left,
    Up,
    Down,
    RightUp,
    RightDown,
    LeftUp,
    LeftDown,
    RightUpRight,
    RightDownRight,
    LeftUpLeft,
    LeftDownLeft,
    UpRightUp,
    UpLeftUp,
    DownRightDown,
    DownLeftDown,
    ZigzagRightUp,
    ZigzagRightDown,
    ZigzagLeftUp,
    ZigzagLeftDown,
    ZigzagUpRight,
    ZigzagUpLeft,
    ZigzagDownRight,
    ZigzagDownLeft
}

public delegate void NoBoardEventHandler(object sender, NoBoardEventArgs e);

public class NoBoardEventArgs : EventArgs
{
    public string NoBoardMessage { get; private set; }

    public NoBoardEventArgs(string message)
    {
        NoBoardMessage = message;
    }
}

public class BoardBuilder
{
    public event NoBoardEventHandler NoBoardEvent;

    private enum _directions { Right, RightUp, RightDown, Left, LeftUp, LeftDown, Up, Down, None }

    private Dictionary<CodeShapes, _directions[]> _SHAPEDIRECTION = new Dictionary<CodeShapes, _directions[]>
    {
        { CodeShapes.Right, new _directions[]{ _directions.Right, _directions.Right, _directions.Right } },
        { CodeShapes.Left, new _directions[]{ _directions.Left, _directions.Left, _directions.Left } },
        { CodeShapes.Up, new _directions[]{ _directions.Up, _directions.Up, _directions.Up } },
        { CodeShapes.Down, new _directions[]{ _directions.Down, _directions.Down, _directions.Down } },
        { CodeShapes.RightUp, new _directions[]{ _directions.RightUp, _directions.RightUp, _directions.RightUp} },
        { CodeShapes.RightDown, new _directions[]{ _directions.RightDown, _directions.RightDown, _directions.RightDown } },
        { CodeShapes.LeftUp, new _directions[]{ _directions.LeftUp, _directions.LeftUp, _directions.LeftUp } },
        { CodeShapes.LeftDown, new _directions[]{ _directions.LeftDown, _directions.LeftDown, _directions.LeftDown } },
        { CodeShapes.RightUpRight, new _directions[]{ _directions.Right, _directions.Up, _directions.Right } },
        { CodeShapes.RightDownRight, new _directions[]{ _directions.Right, _directions.Down, _directions.Right } },
        { CodeShapes.LeftUpLeft, new _directions[]{ _directions.Left, _directions.Up, _directions.Left } },
        { CodeShapes.LeftDownLeft, new _directions[]{ _directions.Left, _directions.Down, _directions.Left } },
        { CodeShapes.UpRightUp, new _directions[]{ _directions.Up, _directions.Right, _directions.Up } },
        { CodeShapes.UpLeftUp, new _directions[]{ _directions.Up, _directions.Left, _directions.Up } },
        { CodeShapes.DownRightDown, new _directions[]{  _directions.Down, _directions.Right, _directions.Down } },
        { CodeShapes.DownLeftDown, new _directions[]{ _directions.Down, _directions.Left, _directions.Down } },
        { CodeShapes.ZigzagRightUp, new _directions[]{ _directions.RightUp, _directions.RightDown, _directions.RightUp } },
        { CodeShapes.ZigzagRightDown, new _directions[]{ _directions.RightDown, _directions.RightUp, _directions.RightDown } },
        { CodeShapes.ZigzagLeftUp, new _directions[]{ _directions.LeftUp, _directions.LeftDown, _directions.LeftUp } },
        { CodeShapes.ZigzagLeftDown, new _directions[]{ _directions.LeftDown, _directions.LeftUp, _directions.LeftDown } },
        { CodeShapes.ZigzagUpRight, new _directions[]{ _directions.RightUp, _directions.LeftUp, _directions.RightUp } },
        { CodeShapes.ZigzagUpLeft, new _directions[]{ _directions.LeftUp, _directions.RightUp, _directions.LeftUp } },
        { CodeShapes.ZigzagDownRight, new _directions[]{ _directions.RightDown, _directions.LeftDown, _directions.RightDown } },
        { CodeShapes.ZigzagDownLeft, new _directions[]{ _directions.LeftDown, _directions.RightDown, _directions.LeftDown } }
    };

    public List<int> CodePositions { get; private set; }
    public List<int> Code { get; private set; }
    public CodeShapes? CodeShape { get; private set; }

    private Dictionary<int, int> _boardNumbers = new Dictionary<int, int>();
    private int _boardColumns;
    private int _boardRows;
    private List<int> _currentCode = new List<int>();
    private List<int> _codePositions = new List<int>();
    private List<CodeShapes> _shapes;
    private int _fakeCodesNumber;

    private Random _random = new Random();
    private List<int> _allPositions;
    private List<int> _allValues;

    public BoardBuilder(int boardColumns, int boardRows)
    {
        _boardColumns = boardColumns;
        _boardRows = boardRows;

        _allPositions = Enumerable.Range(0, _boardColumns * _boardRows).ToList();
        _allValues = Enumerable.Range(0, 100).ToList();
    }

    public Dictionary<int, int> CreateBoard(int codeLenth, bool isNew, List<CodeShapes> shapes, int fakeCodesNumber, int codeValuesNumber)
    {
        if (Code == null || isNew)
        {
            Code = CreateCode(codeLenth);
        }
        _boardNumbers.Clear();
        _shapes = shapes;
        _fakeCodesNumber = fakeCodesNumber;
        CodeShape = null;

        _codePositions.Clear();

        _currentCode = Code;
        FindPositions();

        var codeValuesCounter = 4;

        CodePositions = _boardNumbers.Keys.ToList();

        for (int i = 0; i < _fakeCodesNumber; i++)
        {
            _currentCode = CreateFakeCode();
            FindPositions();
            codeValuesCounter += 3;
        }

        var takenPos = _boardNumbers.Keys.ToList();

        while (codeValuesCounter < codeValuesNumber)
        {
            var pos = RandomPositionBut(takenPos);
            var val = FreeCodeValue(pos);
            if (val != -1)
            {
                _boardNumbers[pos] = val;
                takenPos.Add(pos);
                codeValuesCounter++;
            }
        }

        for (int i = 0; i < _boardColumns * _boardRows; i++)
        {
            if (_boardNumbers.ContainsKey(i))
            {
                continue;
            }
            _boardNumbers[i] = RandomValueBut(Code);
        }

        return _boardNumbers;
    }

    private List<int> CreateCode(int codeLength)
    {
        var code = new List<int>();
        for (int i = 0; i < codeLength; i++)
        {
            var num = _random.Next(100);
            while (code.FindAll(n => n == num).Count >= 2)
            {
                num = _random.Next(100);
            }
            code.Add(num);
        }
        return code;
    }

    private int FreeCodeValue(int pos)
    {
        var codeIndex = _random.Next(Code.Count);
        for (int i = 0; i < Code.Count; i++)
        {
            if (!IsOptionalsCodesAround(pos, codeIndex))
            {
                return Code[codeIndex];
            }
            else
            {
                codeIndex = (codeIndex + 1) % Code.Count;
            }
        }
        return -1;
    }

    private List<int> CreateFakeCode()
    {
        var fakeCode = new List<int>();
        //if (_random.Next(2) == 0)
        //{
        //    fakeCode.Add(RandomValueBut(Code));
        //    for (var i = 1; i < Code.Count; i++)
        //    {
        //        fakeCode.Add(Code[i]);
        //    }
        //}
        //else
        //{
        //    for (var i = 0; i < Code.Count - 1; i++)
        //    {
        //        fakeCode.Add(Code[i]);
        //    }
        //    fakeCode.Add(RandomValueBut(Code));
        //}
        for (var i = 0; i < Code.Count - 1; i++)
        {
            fakeCode.Add(Code[i]);
        }
        fakeCode.Add(RandomValueBut(Code));
        return fakeCode;
    }

    private void FindPositions()
    {
        var shape = _shapes[_random.Next(_shapes.Count)];
        if (CodeShape == null)
        {
            CodeShape = shape;
        }
        var allPos = Enumerable.Range(0, _boardColumns * _boardRows).ToList();

        int firstPos = _boardRows * _boardColumns;
        List<int> positions;

        do
        {
            if (firstPos < _boardColumns * _boardRows)
            {
                allPos.Remove(firstPos);
            }
            if (allPos.Count == 0)
            {
                _shapes.Remove(shape);
                Debug.Log("Remove shape");
                if (_shapes.Count == 0)
                {
                    NoBoardEvent?.Invoke(this, new NoBoardEventArgs("Can't create board"));
                    return;
                }
                shape = _shapes[_random.Next(_shapes.Count)];
                allPos = Enumerable.Range(0, _boardColumns * _boardRows).ToList();
            }
            var posIndex = _random.Next(allPos.Count);
            firstPos = allPos[posIndex];
            positions = GetPositionsByOnePos(shape, firstPos, 0);
        }
        while (positions == null || IsOnOtherCode(shape, positions, false) || IsOptionalsCodesAroundPostiotions(positions, shape));

        if (_codePositions.Count == 0)
        {
            _codePositions = positions;
        }

        for (int i = 0; i < positions.Count; i++)
        {
            Debug.Log(positions[i]);
            if (!_boardNumbers.ContainsKey(positions[i]))
            {
                _boardNumbers.Add(positions[i], _currentCode[i]);
            }
        }
        Debug.Log(shape);
    }

    private List<int> GetPositionsByOnePos(CodeShapes shape, int onePos, int posIndex)
    {
        var positions = new List<int>() { onePos };
        var pos = onePos;
        for (int i = posIndex - 1; i >= 0; i--)
        {
            switch (_SHAPEDIRECTION[shape][i])
            {
                case _directions.Right:
                    pos--;
                    break;
                case _directions.Left:
                    pos++;
                    break;
                case _directions.Up:
                    pos += _boardColumns;
                    break;
                case _directions.Down:
                    pos -= _boardColumns;
                    break;
                case _directions.RightUp:
                    pos = pos - 1 + _boardColumns;
                    break;
                case _directions.RightDown:
                    pos = pos - 1 - _boardColumns;
                    break;
                case _directions.LeftUp:
                    pos = pos + 1 + _boardColumns;
                    break;
                case _directions.LeftDown:
                    pos = pos + 1 - _boardColumns;
                    break;
            }
            if (IsOptionalPos(_SHAPEDIRECTION[shape][i], pos))
            {
                positions.Add(pos);
            }
        }
        if (positions.Count <= posIndex)
        {
            return null;
        }

        positions.Reverse();
        pos = onePos;

        for (int i = posIndex; i < _SHAPEDIRECTION[shape].Length; i++)
        {
            switch (_SHAPEDIRECTION[shape][i])
            {
                case _directions.Right:
                    pos++;
                    break;
                case _directions.Left:
                    pos--;
                    break;
                case _directions.Up:
                    pos -= _boardColumns;
                    break;
                case _directions.Down:
                    pos += _boardColumns;
                    break;
                case _directions.RightUp:
                    pos = pos + 1 - _boardColumns;
                    break;
                case _directions.RightDown:
                    pos = pos + 1 + _boardColumns;
                    break;
                case _directions.LeftUp:
                    pos = pos - 1 - _boardColumns;
                    break;
                case _directions.LeftDown:
                    pos = pos - 1 + _boardColumns;
                    break;
            }
            if (IsOptionalPos(_SHAPEDIRECTION[shape][i], pos))
            {
                positions.Add(pos);
            }
        }
        if (positions.Count <= _SHAPEDIRECTION[shape].Length)
        {
            return null;
        }

        return positions;
    }

    private bool IsOptionalPos(_directions direction, int pos)
    {
        if (pos < 0 || pos >= _boardColumns * _boardRows)
        {
            return false;
        }

        switch (direction)
        {
            case _directions.Right:
                if (pos % _boardColumns == 0)
                {
                    return false;
                }
                break;
            case _directions.Left:
                if (pos % _boardColumns == _boardColumns - 1)
                {
                    return false;
                }
                break;
            case _directions.Up:
                if (pos < 0)
                {
                    return false;
                }
                break;
            case _directions.Down:
                if (pos > (_boardColumns * _boardRows) - 1)
                {
                    return false;
                }
                break;
            case _directions.RightUp:
                if (pos % _boardColumns == 0 || pos < 0)
                {
                    return false;
                }
                break;
            case _directions.RightDown:
                if (pos % _boardColumns == 0 || pos > (_boardColumns * _boardRows) - 1)
                {
                    return false;
                }
                break;
            case _directions.LeftUp:
                if (pos % _boardColumns == _boardColumns - 1 || pos < 0)
                {
                    return false;
                }
                break;
            case _directions.LeftDown:
                if (pos % _boardColumns == _boardColumns - 1 || pos > (_boardColumns * _boardRows) - 1)
                {
                    return false;
                }
                break;
        }
        return true;
    }

    private bool IsOptionalsCodesAroundPostiotions(List<int> postiotions, CodeShapes? mainShape)
    {
        for (int i = 0; i < postiotions.Count; i++)
        {
            if (IsOptionalsCodesAround(postiotions[i], i, mainShape))
            {
                return true;
            }
        }
        return false;
    }

    private bool IsOptionalsCodesAround(int position, int posIndex, CodeShapes? mainShape = null)
    {
        foreach (CodeShapes codeShape in Enum.GetValues(typeof(CodeShapes)))
        {
            if (codeShape == mainShape)
            {
                continue;
            }
            var optionalCode = GetPositionsByOnePos(codeShape, position, posIndex);
            if (optionalCode != null)
            {
                if (IsOnOtherCode(codeShape, optionalCode, true))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private bool IsOnOtherCode(CodeShapes shape, List<int> positions, bool isJustForCheck)
    {
        if (_boardNumbers.ContainsKey(positions[0]))
        {
            if (_boardNumbers[positions[0]] == _currentCode[0])
            {
                var secondAround = NumberAround(positions[1], _currentCode[1]);
                var thirdAround = NumberAround(positions[2], _currentCode[2]);
                if (secondAround.Contains(_SHAPEDIRECTION[shape][0]) && thirdAround.Contains(_SHAPEDIRECTION[shape][1]))
                {
                    return true;
                }
            }
            else if (!isJustForCheck)
            {
                return true;
            }
        }
        if (_boardNumbers.ContainsKey(positions[positions.Count - 1]))
        {
            if (_boardNumbers[positions[positions.Count - 1]] == Code[Code.Count - 1])
            {
                var secondAround = NumberAround(positions[1], _currentCode[1]);
                var thirdAround = NumberAround(positions[2], _currentCode[2]);
                if (secondAround.Contains(BakcDirection(_SHAPEDIRECTION[shape][0])) && thirdAround.Contains(BakcDirection(_SHAPEDIRECTION[shape][1])))
                {
                    return true;
                }
            }
            else if (!isJustForCheck)
            {
                return true;
            }
            if (_codePositions.Count == positions.Count && _codePositions[_codePositions.Count - 1] == (positions[positions.Count - 1]))
            {
                return true;
            }
        }
        return false;
    }

    private List<_directions> NumberAround(int pos, int number)
    {
        var directions = new List<_directions>();
        if (_boardNumbers.ContainsKey(pos - 1) && _boardNumbers[pos - 1] == number)
        {
            directions.Add(_directions.Left);
        }
        if (_boardNumbers.ContainsKey(pos + 1) && _boardNumbers[pos + 1] == number)
        {
            directions.Add(_directions.Right);
        }
        if (_boardNumbers.ContainsKey(pos - _boardColumns) && _boardNumbers[pos - _boardColumns] == number)
        {
            directions.Add(_directions.Up);
        }
        if (_boardNumbers.ContainsKey(pos + _boardColumns) && _boardNumbers[pos + _boardColumns] == number)
        {
            directions.Add(_directions.Down);
        }
        if (_boardNumbers.ContainsKey(pos - 1 - _boardColumns) && _boardNumbers[pos - 1 - _boardColumns] == number)
        {
            directions.Add(_directions.LeftUp);
        }
        if (_boardNumbers.ContainsKey(pos - 1 + _boardColumns) && _boardNumbers[pos - 1 + _boardColumns] == number)
        {
            directions.Add(_directions.LeftDown);
        }
        if (_boardNumbers.ContainsKey(pos + 1 - _boardColumns) && _boardNumbers[pos + 1 - _boardColumns] == number)
        {
            directions.Add(_directions.RightUp);
        }
        if (_boardNumbers.ContainsKey(pos + 1 + _boardColumns) && _boardNumbers[pos + 1 + _boardColumns] == number)
        {
            directions.Add(_directions.RightDown);
        }

        return directions;
    }

    //private int RandomNumberBut(List<int> butNumbers)
    //{
    //    var rnd = _random.Next(_boardColumns * _boardRows);
    //    while (butNumbers.Contains(rnd))
    //    {
    //        rnd = _random.Next(_boardColumns * _boardRows);
    //    }

    //    return rnd;
    //}

    private int RandomPositionBut(List<int> butPositions)
    {
        foreach (var butPos in butPositions)
        {
            _allPositions.Remove(butPos);
        }
        var rnd = _allPositions[_random.Next(_allPositions.Count)];
        _allPositions = Enumerable.Range(0, _boardColumns * _boardRows).ToList();
        return rnd;
    }

    private int RandomValueBut(List<int> butValues)
    {
        foreach (var butVal in butValues)
        {
            _allValues.Remove(butVal);
        }
        var rnd = _allValues[_random.Next(_allValues.Count)];
        _allValues = Enumerable.Range(0, 100).ToList();
        return rnd;
    }

    private _directions BakcDirection(_directions direction)
    {
        switch (direction)
        {
            case _directions.Right:
                return _directions.Left;
            case _directions.Left:
                return _directions.Right;
            case _directions.Up:
                return _directions.Down;
            case _directions.Down:
                return _directions.Up;
            case _directions.RightUp:
                return _directions.LeftDown;
            case _directions.RightDown:
                return _directions.LeftUp;
            case _directions.LeftUp:
                return _directions.RightDown;
            case _directions.LeftDown:
                return _directions.RightUp;
            default:
                return _directions.None;
        }
    }

    public List<int> GetThirdWithoutCode()
    {
        var thirdWighoutCode = new List<int>();

        var third = Math.Round(_boardRows / 3d);

        var codeColumns = _codePositions.Select(c => c / _boardColumns).ToList();
        var columnsWithoutCode = new List<int>();
        for (int i = 0; i < third; i++)
        {
            if (codeColumns.Max() < _boardRows - 1 - third)
            {
                columnsWithoutCode.Add(_boardRows - (i + 1));
            }
            else if (codeColumns.Min() >= third)
            {
                columnsWithoutCode.Add(i);
            }
            else
            {
                if (!codeColumns.Contains(i))
                {
                    columnsWithoutCode.Add(i);
                }
                else
                {
                    columnsWithoutCode.Add(_boardRows - (i - columnsWithoutCode.Count + 1));
                }
            }
        }

        foreach (var key in _boardNumbers.Keys)
        {
            if (columnsWithoutCode.Contains(key / _boardColumns))
            {
                thirdWighoutCode.Add(key);
            }
        }

        return thirdWighoutCode;
    }

    public List<int> GetNoneCodeNumber(int digitsNumber)
    {
        var numbersPositions = new List<int>();

        var keys = _boardNumbers.Keys.ToList();

        for (int i = 0; i < digitsNumber; i++)
        {
            do
            {
                if (numbersPositions.Count == i)
                {
                    numbersPositions.Add(keys[_random.Next(keys.Count)]);
                }
                else
                {
                    numbersPositions[i] = keys[_random.Next(keys.Count)];
                }
                keys.Remove(numbersPositions[i]);
            }
            while (Code.Contains(_boardNumbers[numbersPositions[i]]));
        }

        return numbersPositions;
    }

    public List<int> GetFirstCodeDigits()
    {
        return _boardNumbers.Where(kv => kv.Value == Code[0]).Select(kv => kv.Key).ToList();
    }
}
