﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Players
{
    public List<PlayerData> PlayersData;
}

[Serializable]
public class PlayerData
{
    public string Uid;
    public string Name;
    public string ImageUrl;
    public int AvatarIndex;
    public string Degree;
    public bool IsBot;
}
