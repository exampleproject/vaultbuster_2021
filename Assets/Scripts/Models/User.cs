﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    public string Id;
    public string Name;
    public ImageParams Image;
    public VaultParams Vault;
    public int XP;
    public int WorldIndex;
    public string IsUnderAttack;
    public string Tokens;
    public List<string> TokensList;
    public UserGamesProgressData UserGamesProgress;
    public string Revenges;
    public List<Revenge> RevengesList;
    public string SocialId;
    public string NextDaily;
}

[Serializable]
public class ImageParams
{
    public int Width;
    public int Height;
    public string ImageExtention;
    public string ImageUrl;
    public int AvatarIndex;
}

[Serializable]
public class VaultParams
{
    public int GoldBars;
    public int Coins;
    public int CoinsPercentsCanTaken;
    public int Games;
    public int Boosters;
    public int Shields;
    public List<string> Assets;
}

[Serializable]
public class UserGamesProgressData
{
    public int TotalGames;
    public int TotalWins;
    public int CurrentGames;
    public int CurrentWins;
}

[Serializable]
public class Revenge
{
    public PlayerData Attacker;
    public int StolenSum;
}