﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyBonusManager : MonoBehaviour
{
    public TMPro.TextMeshProUGUI Cell1Text;
    public TMPro.TextMeshProUGUI Cell2Text;
    public TMPro.TextMeshProUGUI Cell3Text;
    public TMPro.TextMeshProUGUI Cell4Text;
    public TMPro.TextMeshProUGUI Cell5Text;
    public TMPro.TextMeshProUGUI Cell6Text;
    public TMPro.TextMeshProUGUI Cell7Text;
    public TMPro.TextMeshProUGUI Cell8Text;

    private Dictionary<int, Dictionary<string, int>> _dailyBonusData;
    private Dictionary<string, int> _currentOption;

    private enum _worldMultiply { First = 1, Second = 5, Third = 25, Fourth = 40, Fifth = 100, Sixth = 200, Seventh = 500 }

    private void Start()
    {
        DatabaseManager.Database.GotDailyBonus.AddListener(OnGotDailyBonusData);
    }

    private void OnGotDailyBonusData(Dictionary<int, Dictionary<string, int>> data)
    {
        _dailyBonusData = data;
    }

    public void ResetWheelValues()
    {
        StartCoroutine(WheelValues());
    }

    IEnumerator WheelValues()
    {
        while (AppManager.App == null || GameProgressConfig.GameProgress == null)
        {
            yield return null;
        }
        while (_dailyBonusData == null || AppManager.App.CurrentUser == null || GameProgressConfig.GameProgress.GameProgressData.Count == 0)
        {
            yield return null;
        }

        var world = GameProgressConfig.GameProgress.GetCurrentXPData().World;
        int multiplyer;
        switch (world)
        {
            case 1:
                multiplyer = (int)_worldMultiply.First;
                break;
            case 2:
                multiplyer = (int)_worldMultiply.Second;
                break;
            case 3:
                multiplyer = (int)_worldMultiply.Third;
                break;
            case 4:
                multiplyer = (int)_worldMultiply.Fourth;
                break;
            case 5:
                multiplyer = (int)_worldMultiply.Fifth;
                break;
            case 6:
                multiplyer = (int)_worldMultiply.Sixth;
                break;
            case 7:
                multiplyer = (int)_worldMultiply.Seventh;
                break;
            default:
                multiplyer = 1;
                break;
        }
        
        _currentOption = _dailyBonusData[Random.Range(1, _dailyBonusData.Count)];
        _currentOption["coins"] *= multiplyer;
        _currentOption["coins_2"] *= multiplyer;
        
        Cell1Text.text = "X" + _currentOption["gold_bars"];
        Cell2Text.text = "X" + _currentOption["coins"];
        Cell3Text.text = "X" + _currentOption["shields"];
        Cell4Text.text = "X" + _currentOption["boosters"];
        Cell5Text.text = "X" + _currentOption["gold_bars_2"];
        Cell6Text.text = "X" + _currentOption["coins_2"];
        Cell7Text.text = "X" + _currentOption["boosters_2"];
        Cell8Text.text = "X" + _currentOption["games"];
    }

    public int GetWinSum(int i)
    {
        if (_currentOption != null)
        {
            switch (i)
            {
                case 0:
                    return _currentOption["gold_bars"];
                case 1:
                    return _currentOption["coins"];
                case 2:
                    return _currentOption["shields"];
                case 3:
                    return _currentOption["boosters"];
                case 4:
                    return _currentOption["gold_bars_2"];
                case 5:
                    return _currentOption["coins_2"];
                case 6:
                    return _currentOption["boosters_2"];
                case 7:
                    return _currentOption["games"];
                default:
                    return 0;
            }
        }
        return 0;
    }

    public bool IsDailyBonusData()
    {
        return _dailyBonusData != null;
    }
}
