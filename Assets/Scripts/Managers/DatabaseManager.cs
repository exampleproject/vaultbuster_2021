﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Firebase;
using Firebase.Extensions;
using Firebase.Database;
//using Firebase.Unity.Editor;
using System.Globalization;

public class DatabaseManager : MonoBehaviour
{
    public enum UserDataKeys { GoldBars, Coins, CoinsPercentsCanTaken, Games, Boosters, Shields, TotalGames, TotalWins, CurrentGames, CurrentWins, IsUnderAttack, XP, NextDaily, SocilId };

    public List<User> Users { get; private set; } = new List<User>();
    public UnityEventUser GotUsers;
    public UnityEventBool IsReactUnderAttack;
    public UnityEventDictionaryStringIntFloat GotShop;
    public UnityEventDictionaryIntStringInt GotDailyBonus;

    private DatabaseReference _userData;
    private string _token;

    public static DatabaseManager Database;

    private void Awake()
    {
        if (Database == null)
        {
            Database = this;
            return;
        }
        Destroy(gameObject);
    }

    public void StartDatabase(string userId)
    {
        InitializeFirebase(userId);
    }

    private void InitializeFirebase(string userId)
    {
        Debug.Log("Initailize");

        FirebaseApp app = FirebaseApp.DefaultInstance;
        //app.SetEditorDatabaseUrl("https://vault-buster.firebaseio.com/");
        //if (app.Options.DatabaseUrl != null)
        //{
        //    app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);
        //}

        app.Options.DatabaseUrl = new Uri("https://vault-buster.firebaseio.com/");

        GetCurrentUser(userId);

        GetGameProgressConfig();
    }

    private void GetCurrentUser(string userId)
    {
        if (_userData == null)
        {
            _userData = FirebaseDatabase.DefaultInstance.GetReference("Users/" + userId);
        }

        Debug.Log(_userData);
        _userData.GetValueAsync().ContinueWithOnMainThread(snapshot =>
        {
            if (snapshot.Result.Exists)
            {
                var parseUser = JsonUtility.FromJson<User>(snapshot.Result.GetRawJsonValue());
                parseUser.TokensList = ParseTokens(snapshot.Result);
                parseUser.RevengesList = ParseRevenges(snapshot.Result);
                GetShopConfig();
                GotUsers.Invoke(parseUser);
            }
            else
            {
                GotUsers.Invoke(null);
            }
        });
    }

    private void GetGameProgressConfig()
    {
        DatabaseReference gameProgressData;
        if (AppManager.App.IsDebugMode)
        {
            gameProgressData = FirebaseDatabase.DefaultInstance.GetReference("DebugConfig");
        }
        else
        {
            gameProgressData = FirebaseDatabase.DefaultInstance.GetReference("Config");
        }
        gameProgressData.GetValueAsync().ContinueWith(snapshot =>
        {
            if (snapshot.Result.Exists)
            {
                for (var i = 1; i <= snapshot.Result.ChildrenCount; i++)
                {
                    var xpJson = snapshot.Result.Child(i.ToString()).GetRawJsonValue();
                    var xp = JsonUtility.FromJson<XPData>(xpJson);
                    GameProgressConfig.GameProgress.GameProgressData.Add(xp);
                }
            }
            else
            {
                Debug.Log("Faild Get Game Progress");
            }
        });
    }

    private void GetShopConfig()
    {
        DatabaseReference shopDatabase = FirebaseDatabase.DefaultInstance.GetReference("Shop");
        shopDatabase.GetValueAsync().ContinueWith(snapshot =>
        {
            if (snapshot.Result.Exists)
            {
                GetDailyBonusConfig();
                GotShop.Invoke(ParseShop(snapshot.Result));
            }
            else
            {
                GetShopConfig();
                GotShop.Invoke(null);
            }
        });
    }

    private void GetDailyBonusConfig()
    {
        DatabaseReference dailyBonusDatabase = FirebaseDatabase.DefaultInstance.GetReference("DailyBonus");
        dailyBonusDatabase.GetValueAsync().ContinueWith(snapshot =>
        {
            if (snapshot.Result.Exists)
            {
                GotDailyBonus.Invoke(ParseDailyBonus(snapshot.Result));
            }
            else
            {
                GetDailyBonusConfig();
                GotDailyBonus.Invoke(null);
            }
        });
    }

    public void UpdateUserDataPrameters(Dictionary<string, object> updateValue)
    {
        if (_userData != null)
        {
            _userData.UpdateChildrenAsync(updateValue).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Debug.Log("Root Error:");
                    Debug.Log(task.Exception);
                }
                else if (task.IsCompleted)
                {
                    GetCurrentUser(string.Empty);
                }
            });
        }
    }

    public void CheckIsUnderAttack()
    {
        FirebaseDatabase.DefaultInstance.GetReference("Users/" + AppManager.App.CurrentUser.Id + "/" + GetPathFromKey(UserDataKeys.IsUnderAttack)).GetValueAsync().ContinueWith(task => 
        {
            if (task.IsCompleted)
            {
                Debug.Log("foo");
                AppManager.App.CurrentUser.IsUnderAttack = (string)task.Result.GetValue(true);
                IsReactUnderAttack.Invoke(AppManager.App.CurrentUser.IsUnderAttack == "true");
                if (AppManager.App.CurrentUser.IsUnderAttack == "true")
                {
                    UpdateUserDataPrameters(new Dictionary<string, object> { { Database.GetPathFromKey(UserDataKeys.IsUnderAttack), "false" } });
                }
            }
        });
    }

    public string CreateUserId()
    {
        return FirebaseDatabase.DefaultInstance.GetReference("Users").Push().Key;
    }

    public void AddUser()
    {
        FirebaseDatabase.DefaultInstance.GetReference("Users").Child(AppManager.App.CurrentUser.Id).SetRawJsonValueAsync(JsonUtility.ToJson(AppManager.App.CurrentUser)).ContinueWith(task => UpdateTokens());
        GotUsers.Invoke(AppManager.App.CurrentUser);
    }

    public void SetTokens(string tokens)
    {
        _token = tokens;
    }

    public void UpdateTokens()
    {
        if (!string.IsNullOrEmpty(_token))
        {
            if (!AppManager.App.CurrentUser.TokensList.Contains(_token))
            {
                AppManager.App.CurrentUser.TokensList.Add(_token);
                _userData.Child("Tokens").Push().SetValueAsync(_token);
            }
        }
    }

    public void UpdateRevenges()
    {
        _userData.Child("Revenges").SetValueAsync(null).ContinueWith(task => {
            foreach (var revenge in AppManager.App.CurrentUser.RevengesList)
            {
                _userData.Child("Revenges").Push().SetValueAsync(revenge);
            }
        });
    }

    private List<string> ParseTokens(DataSnapshot userData)
    {
        var tokens = new List<string>();
        foreach (var token in userData.Child("Tokens").Children)
        {
            tokens.Add((string)token.Value);
        }
        return tokens;
    }

    private List<Revenge> ParseRevenges(DataSnapshot userData)
    {
        var revenges = new List<Revenge>();
        foreach(var revenge in userData.Child("Revenges").Children)
        {
            var attacker = JsonUtility.FromJson<Revenge>(revenge.GetRawJsonValue());
            revenges.Add(attacker);
        }
        return revenges;
    }

    private Dictionary<string, Dictionary<int, float>> ParseShop(DataSnapshot shopData)
    {
        var shopPrices = new Dictionary<string, Dictionary<int, float>>();

        foreach (var prodact in shopData.Children)
        {
            var prodactPrices = new Dictionary<int, float>();
            foreach (var amount in prodact.Children)
            {
                var priceValue = amount.GetValue(true);
                float price = Convert.ToSingle(priceValue);
                prodactPrices.Add(Convert.ToInt32(amount.Key), price);
            }
            shopPrices.Add(prodact.Key, prodactPrices);
        }

        return shopPrices;
    }

    private Dictionary<int, Dictionary<string, int>> ParseDailyBonus(DataSnapshot dailyBonusData)
    {
        var dailyBonus = new Dictionary<int, Dictionary<string, int>>();

        foreach (var worldIndex in dailyBonusData.Children)
        {
            var worldDailyBonus = new Dictionary<string, int>();
            foreach (var prize in worldIndex.Children)
            {
                worldDailyBonus.Add(prize.Key, Convert.ToInt32(prize.GetValue(true)));
            }
            dailyBonus.Add(Convert.ToInt32(worldIndex.Key), worldDailyBonus);
        }

        return dailyBonus;
    }

    public string GetPathFromKey(UserDataKeys key)
    {
        switch (key)
        {
            case UserDataKeys.GoldBars:
                return "Vault/GoldBars";
            case UserDataKeys.Games:
                return "Vault/Games";
            case UserDataKeys.Coins:
                return "Vault/Coins";
            case UserDataKeys.Boosters:
                return "Vault/Boosters";
            case UserDataKeys.Shields:
                return "Vault/Shields";
            case UserDataKeys.TotalGames:
                return "UserGamesProgress/TotalGames";
            case UserDataKeys.TotalWins:
                return "UserGamesProgress/TotalWins";
            case UserDataKeys.CurrentGames:
                return "UserGamesProgress/CurrentGames";
            case UserDataKeys.CurrentWins:
                return "UserGamesProgress/CurrentWins";
            case UserDataKeys.IsUnderAttack:
                return "IsUnderAttack";
            case UserDataKeys.XP:
                return "XP";
            case UserDataKeys.NextDaily:
                return "NextDaily";
            case UserDataKeys.SocilId:
                return "SocialId";
            default:
                return string.Empty;
        }
    }

    public object GetObjectByKey(UserDataKeys key)
    {
        switch (key)
        {
            case UserDataKeys.GoldBars:
                return AppManager.App.CurrentUser.Vault.GoldBars;
            case UserDataKeys.Games:
                return AppManager.App.CurrentUser.Vault.Games;
            case UserDataKeys.Coins:
                return AppManager.App.CurrentUser.Vault.Coins;
            case UserDataKeys.Boosters:
                return AppManager.App.CurrentUser.Vault.Boosters;
            case UserDataKeys.Shields:
                return AppManager.App.CurrentUser.Vault.Shields;
            case UserDataKeys.TotalGames:
                return AppManager.App.CurrentUser.UserGamesProgress.TotalGames;
            case UserDataKeys.TotalWins:
                return AppManager.App.CurrentUser.UserGamesProgress.TotalWins;
            case UserDataKeys.CurrentGames:
                return AppManager.App.CurrentUser.UserGamesProgress.CurrentGames;
            case UserDataKeys.CurrentWins:
                return AppManager.App.CurrentUser.UserGamesProgress.CurrentWins;
            case UserDataKeys.IsUnderAttack:
                return AppManager.App.CurrentUser.IsUnderAttack;
            case UserDataKeys.XP:
                return AppManager.App.CurrentUser.XP;
            case UserDataKeys.NextDaily:
                return AppManager.App.CurrentUser.NextDaily;
            case UserDataKeys.SocilId:
                return AppManager.App.CurrentUser.SocialId;
            default:
                return null;
        }
    }
}