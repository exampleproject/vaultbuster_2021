﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TopbarManager : MonoBehaviour
{
    private bool _isGotUser;

    private void Start()
    {
        DatabaseManager.Database.GotUsers.AddListener(OnGotUser);
    }

    private void OnGotUser(User user)
    {
        if (user != null)
        {
            UIReferences.References.TopBar.GoldBullionsCounter.text = AppManager.App.CurrentUser.Vault.GoldBars.ToString();
            UIReferences.References.TopBar.CoinsCounter.text = AppManager.App.CurrentUser.Vault.Coins.ToString();
            UIReferences.References.TopBar.GamesCounter.text = AppManager.App.CurrentUser.Vault.Games.ToString();
            UIReferences.References.TopBar.BoostersCounter.text = AppManager.App.CurrentUser.Vault.Boosters.ToString();
            UIReferences.References.TopBar.ShieldsCounter.text = AppManager.App.CurrentUser.Vault.Shields.ToString();
            UIReferences.References.TopBar.XPCounter.text = (AppManager.App.CurrentUser.XP + 1).ToString();
            UIReferences.References.TopBar.WorldCounter.text = GameProgressConfig.GameProgress.GetCurrentXPData().World.ToString();
            if (!UIReferences.References.TopBar.TopBarPanel.gameObject.activeInHierarchy)
            {
                UIReferences.References.TopBar.TopBarPanel.gameObject.SetActive(true);
            }
        }
    }

    //public void Count(DatabaseManager.UserDataKeys key, int countTo)
    public void Count(Dictionary<DatabaseManager.UserDataKeys, int> valuesToCount)
    {
        var parameterDict = new Dictionary<string, object>();
        foreach (var key in valuesToCount.Keys.ToList())
        {
            int startCount;
            switch (key)
            {
                case DatabaseManager.UserDataKeys.GoldBars:
                    startCount = AppManager.App.CurrentUser.Vault.GoldBars;
                    break;
                case DatabaseManager.UserDataKeys.Coins:
                    startCount = AppManager.App.CurrentUser.Vault.Coins;
                    break;
                case DatabaseManager.UserDataKeys.Boosters:
                    startCount = AppManager.App.CurrentUser.Vault.Boosters;
                    break;
                case DatabaseManager.UserDataKeys.Games:
                    startCount = AppManager.App.CurrentUser.Vault.Games;
                    break;
                case DatabaseManager.UserDataKeys.Shields:
                    startCount = AppManager.App.CurrentUser.Vault.Shields;
                    break;
                default:
                    startCount = 0;
                    break;
            }
            StartCoroutine(countFromTo(key, valuesToCount[key], startCount));
            valuesToCount[key] += startCount;
            parameterDict.Add(DatabaseManager.Database.GetPathFromKey(key), valuesToCount[key]);
        }
        DatabaseManager.Database.UpdateUserDataPrameters(parameterDict);
    }

    IEnumerator countFromTo(DatabaseManager.UserDataKeys key, int countTo, int startCount)
    {
        TMPro.TextMeshProUGUI counterText;

        switch (key)
        {
            case DatabaseManager.UserDataKeys.GoldBars:
                counterText = UIReferences.References.TopBar.GoldBullionsCounter;
                break;
            case DatabaseManager.UserDataKeys.Coins:
                counterText = UIReferences.References.TopBar.CoinsCounter;
                break;
            case DatabaseManager.UserDataKeys.Boosters:
                counterText = UIReferences.References.TopBar.BoostersCounter;
                break;
            case DatabaseManager.UserDataKeys.Games:
                counterText = UIReferences.References.TopBar.GamesCounter;
                break;
            case DatabaseManager.UserDataKeys.Shields:
                counterText = UIReferences.References.TopBar.ShieldsCounter;
                break;
            default:
                counterText = null;
                break;
        }

        for(float timer = 0; timer < 1; timer += Time.deltaTime)
        {
            counterText.text = (startCount + (int)(countTo * timer)).ToString();
            yield return null;
        }
        counterText.text = (startCount + countTo).ToString();
    }

    public void UpdateTopBar()
    {
        OnGotUser(AppManager.App.CurrentUser);
    }
}
