using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreensManager : MonoBehaviour
{
    public enum Screens
    {
        FIRST_SCREEN,
        CHOOSE_SKILL_LEVEL,
        CHOOSE_PLAYER,
        NEW_USER_SETTINGS,
        WELLCOM_BONUSES,
        BREAKIN,
        TRANSFER_EFFECT,
        REWARD,
        REWARD_OPTIONS,
        DEGREE_POPUP,
        WORLD_POPUP,
        WORLD_HIGHER_POPUP,
        DIRECTIONS,
        CODE_SWIPED,
        TIME_OUT,
        SHOP,
        DAILY_BONUS_WHEEL,
        DAILY_BONUS_COLLECT,
        REACT,
        WAIT_WHEEL
    }

    public enum Requires
    {
        NONE,
        GAME_PROGRESS,
        DAILY_BONUS,
        SHOP,
        TROPHIES
    }

    public Screens NextScreen;
    public Requires WaitingTo;
}
