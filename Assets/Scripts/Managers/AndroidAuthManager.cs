﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Extensions;
using Firebase.Auth;
using Google;

public class AndroidAuthManager : MonoBehaviour
{
    public Text DebugText;
    public bool IsFacebookLogin;
    public string webClientId = "<your client id here>";
    public UnityEventString AndroidLogedIn;

    private FirebaseAuth _auth;

    private GoogleSignInConfiguration _configuration;

    public static AndroidAuthManager AndroidAuthentication;

    private void Awake()
    {
        if (AndroidAuthentication == null)
        {
            AndroidAuthentication = this;
            return;
        }
        Destroy(gameObject);
    }

    public void InitAndroidAuth()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        _configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
        
        SignInWithGoogle();
#endif
    }

    public void SignInWithGoogle()
    {
        _auth = FirebaseAuth.DefaultInstance;

        GoogleSignIn.Configuration = _configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    public void SignOutFromGoogle()
    {
        GoogleSignIn.DefaultInstance.SignOut();
    }

    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                }
                else
                {

                }
            }
        }
        else if (task.IsCanceled)
        {

        }
        else
        {
            //AddToInformation("Welcome: " + task.Result.DisplayName + "!");
            //AddToInformation("Email = " + task.Result.Email);
            //AddToInformation("Google ID Token = " + task.Result.IdToken);
            //AddToInformation("Email = " + task.Result.Email);
            SignInWithGoogleOnFirebase(task.Result.IdToken);
            //DatabaseManager.Database.SetTokens(task.Result.IdToken);
            //AndroidLogedIn.Invoke(task.Result.UserId);
        }
    }

    private void SignInWithGoogleOnFirebase(string idToken)
    {
        Credential credential = null;
        if (IsFacebookLogin)
        {
            FacebookAuthProvider.GetCredential(idToken);
        }
        else
        {
            credential = GoogleAuthProvider.GetCredential(idToken, null);
        }

        if (credential != null)
        {
            _auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
            {
                AggregateException ex = task.Exception;
                if (ex != null)
                {
                    //UIReferences.References.DebugText.text = ex.InnerExceptions[0].Message;
                    if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
                    {

                    }
                }
                else
                {
                    AndroidLogedIn.Invoke(task.Result.UserId);
                }
            });
        }
    }
}
