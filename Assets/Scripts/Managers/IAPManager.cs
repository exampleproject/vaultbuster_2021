﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;


public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    private string _key;
    private Dictionary<string, object> _updateValues = new Dictionary<string, object>();

    public static string GOLD_BARS_6 = "gold_bars_6";
    public static string GOLD_BARS_13 = "gold_bars_13";
    public static string GOLD_BARS_46 = "gold_bars_46";
    public static string GOLD_BARS_125 = "gold_bars_125";
    public static string GOLD_BARS_260 = "gold_bars_260";
    public static string GOLD_BARS_430 = "gold_bars_430";

    private void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
        _key = DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.GoldBars);
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(GOLD_BARS_6, ProductType.Consumable);
        builder.AddProduct(GOLD_BARS_13, ProductType.Consumable);
        builder.AddProduct(GOLD_BARS_46, ProductType.Consumable);
        builder.AddProduct(GOLD_BARS_125, ProductType.Consumable);
        builder.AddProduct(GOLD_BARS_260, ProductType.Consumable);
        builder.AddProduct(GOLD_BARS_430, ProductType.Consumable);

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyGoldBars(int i)
    {
        switch (i)
        {
            case 6:
                BuyProductID(GOLD_BARS_6);
                break;
            case 13:
                BuyProductID(GOLD_BARS_13);
                break;
            case 46:
                BuyProductID(GOLD_BARS_46);
                break;
            case 125:
                BuyProductID(GOLD_BARS_125);
                break;
            case 260:
                BuyProductID(GOLD_BARS_260);
                break;
            case 430:
                BuyProductID(GOLD_BARS_430);
                break;
        }
    }

    private void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
                GetGoldBars(productId);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    private void GetGoldBars(string productId)
    {
        if (!_updateValues.ContainsKey(_key))
        {
            _updateValues.Add(_key, AppManager.App.CurrentUser.Vault.GoldBars);
        }
        switch (productId)
        {
            case "gold_bars_6":
                _updateValues[_key] = AppManager.App.CurrentUser.Vault.GoldBars + 6;
                break;
            case "gold_bars_13":
                _updateValues[_key] = AppManager.App.CurrentUser.Vault.GoldBars + 13;
                break;
            case "gold_bars_46":
                _updateValues[_key] = AppManager.App.CurrentUser.Vault.GoldBars + 46;
                break;
            case "gold_bars_125":
                _updateValues[_key] = AppManager.App.CurrentUser.Vault.GoldBars + 125;
                break;
            case "gold_bars_260":
                _updateValues[_key] = AppManager.App.CurrentUser.Vault.GoldBars + 260;
                break;
            case "gold_bars_430":
                _updateValues[_key] = AppManager.App.CurrentUser.Vault.GoldBars + 430;
                break;
        }
        DatabaseManager.Database.UpdateUserDataPrameters(_updateValues);
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        var goldPath = DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.GoldBars);
        if (String.Equals(args.purchasedProduct.definition.id, GOLD_BARS_6, StringComparison.Ordinal))
        {
            Debug.Log("6 gold bars");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>() { { goldPath, AppManager.App.CurrentUser.Vault.GoldBars + 6 } });
        }
        else if (String.Equals(args.purchasedProduct.definition.id, GOLD_BARS_13, StringComparison.Ordinal))
        {
            Debug.Log("13 gold bars");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>() { { goldPath, AppManager.App.CurrentUser.Vault.GoldBars + 13 } });
        }
        else if (String.Equals(args.purchasedProduct.definition.id, GOLD_BARS_46, StringComparison.Ordinal))
        {
            Debug.Log("46 gold bars");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>() { { goldPath, AppManager.App.CurrentUser.Vault.GoldBars + 46 } });
        }
        else if (String.Equals(args.purchasedProduct.definition.id, GOLD_BARS_125, StringComparison.Ordinal))
        {
            Debug.Log("125 gold bars");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>() { { goldPath, AppManager.App.CurrentUser.Vault.GoldBars + 125 } });
        }
        else if (String.Equals(args.purchasedProduct.definition.id, GOLD_BARS_260, StringComparison.Ordinal))
        {
            Debug.Log("260 gold bars");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>() { { goldPath, AppManager.App.CurrentUser.Vault.GoldBars + 260 } });
        }
        else if (String.Equals(args.purchasedProduct.definition.id, GOLD_BARS_430, StringComparison.Ordinal))
        {
            Debug.Log("430 gold bars");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>() { { goldPath, AppManager.App.CurrentUser.Vault.GoldBars + 430 } });
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}