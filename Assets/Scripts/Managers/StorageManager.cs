﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using Firebase.Extensions;
using Firebase.Storage;

public class StorageManager : MonoBehaviour
{
    public UnityEventString ImageUploaded;
    public UnityEventSprite SpriteDownloaded;

    private FirebaseStorage _storage;

    public static StorageManager Storage;

    private void Awake()
    {
        if (Storage == null)
        {
            Storage = this;
            return;
        }
        Destroy(gameObject);
    }

    public void StartStorage()
    {
        _storage = FirebaseStorage.DefaultInstance;
    }

    public void UploadPlayerImage(string fileName, string localFile)
    {
        var rootRef = _storage.RootReference;

        var fileRef = rootRef.Child(string.Format("Images/{0}", fileName));

        fileRef.PutFileAsync(localFile).ContinueWith((Task<StorageMetadata> task) =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
            }
            else
            {
                // Metadata contains file metadata such as size, content-type, and download URL.
                StorageMetadata metadata = task.Result;
                string download_url = metadata.Path;
                Debug.Log("Finished uploading...");
                Debug.Log("download url = " + download_url);
                ImageUploaded.Invoke(download_url);
            }
        });
    }

    public void DownloadPlayerImage(string url)
    {
        var imageReference = _storage.GetReference(url);
        imageReference.GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
            }
            else
            {
                Debug.Log(task.Result);
                //UrlDownloaded.Invoke(task.Result.ToString());
                StartCoroutine(CreateTexture(task.Result.ToString()));
            }
        });
    }

    IEnumerator CreateTexture(string imageUrl)
    {
        Debug.Log(imageUrl);
        var request = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;
            SpriteDownloaded.Invoke(Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero));
        }
    }
}
