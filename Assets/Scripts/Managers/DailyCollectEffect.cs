using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyCollectEffect : MonoBehaviour
{
    public void OnParticleSystemStopped()
    {
        UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_SKILL_LEVEL, ScreensManager.Requires.GAME_PROGRESS);
        gameObject.SetActive(false);
    }
}
