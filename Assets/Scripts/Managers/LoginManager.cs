﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
//using Facebook.Unity;
using Firebase;
using Firebase.Extensions;
using Firebase.Auth;
using Google;

public class LoginManager : MonoBehaviour
{
    public enum LoginPlatforms { Facebook, Google, Email };

    public string webClientId = "<your client id here>";
    public Button FacebookButton;
    public UnityEventFirebaseUser LoginID;

    public LoginPlatforms LoginPlatform { get; private set; }

    private FirebaseAuth _auth;
    private bool _isFirebase;
    private GoogleSignInConfiguration _configuration;

    private Dictionary<string, FirebaseUser> _userByAuth = new Dictionary<string, FirebaseUser>();
    private bool _fetchingToken = false;
    private string _displayName;

    public static LoginManager Login;

    private void Awake()
    {
        if (Login == null)
        {
            //FacebookButton.onClick.AddListener(OnFacebookClick);

            Login = this;
            return;
        }
        Destroy(gameObject);
    }

    private void Start()
    {
        StartCoroutine(WaitForInternet());
    }

    private IEnumerator WaitForInternet()
    {
        while (Application.internetReachability == NetworkReachability.NotReachable)
        {
            yield return null;
        }
        StartCoroutine(WaitForMessgingManager());
        //FB.Init(OnInitComplete);
    }

    private IEnumerator WaitForMessgingManager()
    {
        while (MessagingManager.Messaging == null)
        {
            yield return null;
        }
        MessagingManager.Messaging.FirebaseDependencies.AddListener(OnFirebaseDependencies);
        UIReferences.References.ActiveScreen(ScreensManager.Screens.FIRST_SCREEN, ScreensManager.Requires.NONE);
    }

    private void OnFirebaseDependencies()
    {
        _isFirebase = true;
    }

    //private void OnInitComplete()
    //{
    //    if (AccessToken.CurrentAccessToken == null)
    //    {
    //        UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(false);
    //        UIReferences.References.GameFllow.FirstScreen.gameObject.SetActive(true);
    //        return;
    //    }
    //    if (AccessToken.CurrentAccessToken.TokenString != null)
    //    {
    //        StartCoroutine(WaitForFirebase(AccessToken.CurrentAccessToken.TokenString, LoginPlatforms.Facebook));
    //    }
    //}

    private IEnumerator WaitForFirebase(string accessToken, LoginPlatforms platform)
    {
        while (!_isFirebase)
        {
            yield return null;
        }
        AuthHandler(accessToken, platform);
    }

    private void AuthHandler(string accessToken, LoginPlatforms platform)
    {
        if (_auth == null)
        {
            _auth = FirebaseAuth.DefaultInstance;
        }
        Credential credential;
        switch (platform)
        {
            case LoginPlatforms.Facebook:
                credential = FacebookAuthProvider.GetCredential(accessToken);
                break;
            case LoginPlatforms.Google:
                credential = GoogleAuthProvider.GetCredential(accessToken, null);
                break;
            default:
                credential = null;
                break;
        }

        if (credential != null)
        {
            Debug.Log(credential);
            _auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task =>
            {
                Debug.Log(task.IsFaulted);
                if (task.IsCanceled)
                {
                    UIReferences.References.DebugText.text = "Canceld";
                    return;
                }
                if (task.IsFaulted)
                {
                    UIReferences.References.DebugText.text = "faulted";
                    return;
                }

                switch (platform)
                {
                    case LoginPlatforms.Facebook:
                        UIReferences.References.DebugText.text = _auth.CurrentUser.DisplayName + "\n" + _auth.CurrentUser.PhotoUrl;
                        LoginPlatform = LoginPlatforms.Facebook;
                        break;
                    case LoginPlatforms.Google:
                        LoginPlatform = LoginPlatforms.Google;
                        break;
                }

                Debug.Log("foo");
                LoginID.Invoke(_auth.CurrentUser);
                Debug.Log("Facebook Login Finaly!!!!!!!");
            });
        }
    }

    private void OnFacebookClick()
    {
        //FB.LogInWithReadPermissions(
        //        new List<string>() {
        //        "public_profile",
        //        "email",
        //        "user_friends"
        //        },
        //        OnFacebookLogin);
    }

    //private void OnFacebookLogin(ILoginResult result)
    //{
    //    if (AccessToken.CurrentAccessToken.TokenString != null)
    //    {
    //        StartCoroutine(WaitForFirebase(AccessToken.CurrentAccessToken.TokenString, LoginPlatforms.Facebook));
    //    }
    //}

    public void FacebookSignOut()
    {
        //Debug.Log(AccessToken.CurrentAccessToken);
        //if (AccessToken.CurrentAccessToken != null)
        //{
        //    FB.LogOut();
        //}
    }

    //Android
    public void InitAndroidAuth()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        _configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
        LoginPlatform = LoginPlatforms.Google;
        SignInWithGoogle();
#endif
    }

    public void SignInWithGoogle()
    {
        _auth = FirebaseAuth.DefaultInstance;

        GoogleSignIn.Configuration = _configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    public void SignOutFromGoogle()
    {
        GoogleSignIn.DefaultInstance.SignOut();
    }

    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                }
                else
                {

                }
            }
        }
        else if (task.IsCanceled)
        {
        }
        else
        {
            StartCoroutine(WaitForFirebase(task.Result.IdToken, LoginPlatforms.Google));
        }
    }

    //PC
    // Handle initialization of the necessary firebase modules:
    public void InitializeFirebaseForEmail()
    {
        LoginPlatform = LoginPlatforms.Email;
        Debug.Log("Setting up Firebase Auth");
        if (_auth == null)
        {
            _auth = FirebaseAuth.DefaultInstance;
        }
        _auth.StateChanged += AuthStateChanged;
        _auth.IdTokenChanged += IdTokenChanged;
    }

    public void TryLogin()
    {
        var email = UIReferences.References.Login.EmailField.text;
        var password = UIReferences.References.Login.PasswordField.text;
        if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
        {
            return;
        }
        var credential = EmailAuthProvider.GetCredential(email, password);
        _auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task =>
        {
            Debug.Log(task.IsFaulted);
            if (task.IsCanceled)
            {
                UIReferences.References.DebugText.text = "Canceld";
                return;
            }
            if (task.IsFaulted)
            {
                UIReferences.References.DebugText.text = "faulted";

                _auth.CreateUserWithEmailAndPasswordAsync(email, password);
            }
            return;
        });
    }

    // Track state changes of the auth object.
    void AuthStateChanged(object sender, EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        FirebaseUser user = null;
        if (senderAuth != null)
        {
            _userByAuth.TryGetValue(senderAuth.App.Name, out user);
        }
        if (senderAuth == _auth)
        {
            if (senderAuth.CurrentUser != user)
            {
                bool signedIn = user != senderAuth.CurrentUser && senderAuth.CurrentUser != null;
                if (!signedIn && user != null)
                {
                    Debug.Log("Signed out " + user.UserId);
                }
                user = senderAuth.CurrentUser;
                Debug.Log(user.ProviderData.ToString());
                _userByAuth[senderAuth.App.Name] = user;
                if (signedIn)
                {
                    Debug.Log("Signed in " + user.UserId);
                    LoginID.Invoke(user);
                }
            }
            else
            {
                UIReferences.References.GameFllow.FirstScreen.gameObject.SetActive(false);
                UIReferences.References.Login.EmailLoginScreen.gameObject.SetActive(true);
            }
        }
    }

    // Track ID token changes.
    void IdTokenChanged(object sender, EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        if (senderAuth == _auth && senderAuth.CurrentUser != null && !_fetchingToken)
        {
            senderAuth.CurrentUser.TokenAsync(false).ContinueWithOnMainThread(task =>
            {
                Debug.Log(String.Format("Token[0:8] = {0}", task.Result.Substring(0, 8)));
                DatabaseManager.Database.SetTokens(task.Result);
            });
        }
    }

    private void OnDestroy()
    {
        if (_auth != null)
        {
            _auth.StateChanged -= AuthStateChanged;
            _auth.IdTokenChanged -= IdTokenChanged;
            _auth = null;
        }
    }

    // Log the result of the specified task, returning true if the task
    // completed successfully, false otherwise.
    private bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string authErrorCode = "";
                FirebaseException firebaseEx = exception as FirebaseException;
                if (firebaseEx != null)
                {
                    authErrorCode = String.Format("AuthError.{0}: ",
                      ((AuthError)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(authErrorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    // Create a user with the email and password.
    public void CreateUserWithEmailAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to create user {0}...", email));

        // This passes the current displayName through to HandleCreateUserAsync
        // so that it can be passed to UpdateUserProfile().  displayName will be
        // reset by AuthStateChanged() when the new user is created and signed in.
        string newDisplayName = _displayName;
        Debug.Log(_auth);
        _auth.CreateUserWithEmailAndPasswordAsync(email, password)
          .ContinueWithOnMainThread((task) =>
          {
              if (LogTaskCompletion(task, "User Creation"))
              {
                  LoginID.Invoke(task.Result);
                  return UpdateUserProfileAsync(newDisplayName: newDisplayName);
              }
              return task;
          }).Unwrap();
    }

    // Update the user's display name with the currently selected display name.
    public Task UpdateUserProfileAsync(string newDisplayName = null)
    {
        if (_auth.CurrentUser == null)
        {
            Debug.Log("Not signed in, unable to update user profile");
            return Task.FromResult(0);
        }
        _displayName = newDisplayName ?? _displayName;
        Debug.Log("Updating user profile");
        return _auth.CurrentUser.UpdateUserProfileAsync(new UserProfile
        {
            DisplayName = _displayName,
            PhotoUrl = _auth.CurrentUser.PhotoUrl,
        }).ContinueWithOnMainThread(task =>
        {
            if (LogTaskCompletion(task, "User profile"))
            {
                //EventsManager.Events.NewUser.Invoke();
            }
        });
    }

    // Sign-in with an email and password.
    public Task SigninWithEmailAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to sign in as {0}...", email));
        return _auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(HandleSignInWithUser);
    }

    // This is functionally equivalent to the Signin() function.  However, it
    // illustrates the use of Credentials, which can be aquired from many
    // different sources of authentication.
    public Task SigninWithEmailCredentialAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to sign in as {0}...", email));
        return _auth.SignInWithCredentialAsync(EmailAuthProvider.GetCredential(email, password)).ContinueWithOnMainThread(HandleSignInWithUser);
    }

    // Called when a sign-in without fetching profile data completes.
    private void HandleSignInWithUser(Task<FirebaseUser> task)
    {
        if (LogTaskCompletion(task, "Sign-in"))
        {
            Debug.Log(String.Format("{0} signed in", task.Result.DisplayName));
            Debug.Log(task.Result.UserId);
        }
    }
}
