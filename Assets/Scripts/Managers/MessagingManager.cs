﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Messaging;
using System;
using Firebase;
using Firebase.Extensions;
using Firebase.Analytics;
using System.Globalization;
using UnityEngine.Events;

public class MessagingManager : MonoBehaviour
{
    public UnityEvent FirebaseDependencies;

    public UnityEventBuster UnderAttack;
    public static MessagingManager Messaging;

    private void Awake()
    {
        if (Messaging == null)
        {
            Messaging = this;
            StartFirebase();
            return;
        }
        Destroy(gameObject);
        StartFirebase();
    }

    public void StartFirebase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //InitializeFirebase();
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                FirebaseDependencies.Invoke();
                OnFirebaseDependencies();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void OnFirebaseDependencies()
    {
        Debug.Log("Messaging.................");
        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.MessageReceived += OnMessageReceived;
        FirebaseMessaging.SubscribeAsync("topic").ContinueWithOnMainThread(task =>
        {
            Debug.Log("subcribeAsync");
        });
        FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(task =>
        {
            Debug.Log("RequestPermissionAsync");
        });
    }

    private void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        //Debug.Log("Received Registration Token: " + token.Token);
        //UIReferences.References.DebugText.text = "Received Registration Token: " + token.Token;
        DatabaseManager.Database.SetTokens(token.Token);
    }

    private void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        try
        {
            //Debug.Log("Received a new message from: " + e.Message.From);
            //UIReferences.References.DebugText.text = e.Message.Data["endTime"];
            //UIReferences.References.DebugText.gameObject.SetActive(true);

            string messageType;

            if (!e.Message.Data.TryGetValue("type", out messageType))
            {
                return;
            }
            switch (messageType)
            {
                case "attack-alarm":
                    string endTimeString, senderID, senderName, isBot;
                    if (!e.Message.Data.TryGetValue("endTime", out endTimeString) || !e.Message.Data.TryGetValue("senderID", out senderID) || !e.Message.Data.TryGetValue("senderName", out senderName) || !e.Message.Data.TryGetValue("isBot", out isBot))
                    {
                        break;
                    }
                    DateTime endTime;
                    if (DateTime.TryParseExact(endTimeString, UIReferences.DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AllowInnerWhite, out endTime))
                    {
                        AppManager.App.EndAttackTime = endTime;
                    }
                    UnderAttack.Invoke(new Buster
                    {
                        SenderId = senderID,
                        SenderName = senderName,
                        IsBot = bool.Parse(isBot)
                    });
                    break;
                case "change-code":
                    UIReferences.References.ActiveScreen(ScreensManager.Screens.CODE_SWIPED, ScreensManager.Requires.NONE);
                    break;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Message error " + ex.Message + " Trace: " + ex.StackTrace);
        }
    }
}
