﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase;
using Firebase.Extensions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Firebase.Auth;
using Firebase.Analytics;
using GetSocialSdk.Core;
using GetSocialSdk.Ui;
using System.Linq;

public class AppManager : MonoBehaviour
{
    public bool IsDebugMode;
    public IAPManager IAP;
    public Image TestImage;

    public static AppManager App;

    public User CurrentUser { get; private set; }
    public Buster AttackBuster { get; private set; }
    public PlayerData OpponentPlayer { get; set; }
    public DateTime EndAttackTime { get; set; }
    public int CurrentMinToBust { get; set; }
    public int CurrentMaxToBust { get; set; }
    public List<string> SocialIds { get; private set; }

    private string _userId;
    private bool _isStartGame;
    private bool _isSocialInit;
    private string _referId = "";

    private void Awake()
    {
        if (App == null)
        {
            App = this;
            LoginManager.Login.LoginID.AddListener(OnLogedIn);
            DatabaseManager.Database.GotUsers.AddListener(OnGotUsers);
            MessagingManager.Messaging.UnderAttack.AddListener(OnUnderAttack);
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            TinySauce.OnGameStarted();

            GetSocial.Init();

            GetSocial.AddOnInitializedListener(OnSocialInit);


            return;
        }
        Destroy(gameObject);
    }

    private void OnUnderAttack(Buster buster)
    {
        AttackBuster = buster;
    }

    private void OnLogedIn(FirebaseUser user)
    {
        UIReferences.References.DebugText.text = string.Empty;
        if (UIReferences.References.GameFllow.FirstScreen.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.FirstScreen.gameObject.SetActive(false);
        }
        if (UIReferences.References.Login.EmailLoginScreen.gameObject.activeInHierarchy)
        {
            UIReferences.References.Login.EmailLoginScreen.gameObject.SetActive(false);
        }
        UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(true);
        _userId = user.UserId;

        if (LoginManager.Login.LoginPlatform == LoginManager.LoginPlatforms.Facebook)
        {
            CurrentUser = new User();
            CurrentUser.Id = _userId;

            CurrentUser.Name = user.DisplayName;
            CurrentUser.Image = new ImageParams();
            CurrentUser.Image.ImageUrl = user.PhotoUrl.ToString();
            CurrentUser.Image.AvatarIndex = -1;
        }

        DatabaseManager.Database.StartDatabase(_userId);
        StorageManager.Storage.StartStorage();
        FuncitionsManager.Functions.StartFunctionsManager();
    }

    private void OnGotUsers(User user)
    {
        if (UIReferences.References.GameFllow.WaitWheel.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(false);
        }
        if (user == null)
        {
            if (LoginManager.Login.LoginPlatform == LoginManager.LoginPlatforms.Facebook)
            {
                CreateUser();
            }
            else
            {
                CurrentUser = new User();
                CurrentUser.Id = _userId;
                UIReferences.References.ActiveScreen(ScreensManager.Screens.NEW_USER_SETTINGS, ScreensManager.Requires.NONE);
                _isStartGame = true;
            }
        }
        else
        {
            CurrentUser = user;

            DatabaseManager.Database.UpdateTokens();

            //StorageManager.Storage.DownloadPlayerImage(user.Image.ImageUrl + user.Image.ImageExtention);
            Debug.Log("Is Under Attack: " + CurrentUser.IsUnderAttack);
            CheckAttackTime();
            if (CurrentUser.IsUnderAttack == "true")
            {
                UIReferences.References.ActiveScreen(ScreensManager.Screens.REACT, ScreensManager.Requires.NONE);
            }
            else if (!_isStartGame)
            {
                if (!UIReferences.References.TopBar.TopBarPanel.gameObject.activeInHierarchy)
                {
                    Debug.Log("Start");
                    UIReferences.References.TopBar.TopBarPanel.gameObject.SetActive(true);
                }
                UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_SKILL_LEVEL, ScreensManager.Requires.GAME_PROGRESS);
                _isStartGame = true;
            }
        }

        StartCoroutine(WaitForSocialInit());

        IAP.gameObject.SetActive(true);
    }

    IEnumerator WaitForSocialInit()
    {
        while (!_isSocialInit)
        {
            yield return null;
        }

        DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.SocilId), GetSocial.GetCurrentUser().Id } });

        Invites.SetOnReferralDataReceivedListener((referralData) => {

            UIReferences.References.DebugOnScreen("App started with referral data: " + referralData.ReferrerUserId);
            FuncitionsManager.Functions.SetFriends(referralData.ReferrerUserId);
        });
    }

    private void OnSocialInit()
    {
        _isSocialInit = true;

        var query = ReferralUsersQuery.AllUsers();
        // to return users only for a specific event:
        // var query = ReferralUsersQuery.UsersForEvent("event");
        var pagingQuery = new PagingQuery<ReferralUsersQuery>(query);


        //Invites.GetReferredUsers(pagingQuery,
        //    (result) =>
        //    {
        //        //UIReferences.References.DebugOnScreen(result.Entries.Count.ToString());
        //        SocialIds = result.Entries.Select(e => e.Id).ToList();
        //    },
        //    (error) =>
        //    {
        //        UIReferences.References.DebugOnScreen("error");
        //    });
    }

    public void OnFirstSkip()
    {
        LoginManager.Login.FacebookSignOut();
#if UNITY_EDITOR
        LoginManager.Login.InitializeFirebaseForEmail();
#elif UNITY_ANDROID
        LoginManager.Login.InitAndroidAuth();

#endif
    }

    public void OnEmailLogin()
    {
        LoginManager.Login.CreateUserWithEmailAsync(UIReferences.References.Login.EmailField.text, UIReferences.References.Login.PasswordField.text);
    }

    public void OnChooseAvatar(bool isOn, int avatarIndex)
    {
        if (isOn)
        {
            CurrentUser.Image.AvatarIndex = avatarIndex;
        }
    }

    public void CreateUser()
    {
        if (string.IsNullOrEmpty(CurrentUser.Id))
        {
            CurrentUser.Id = _userId;
        }
        if (UIReferences.References.Login.UserSettingsScreen.gameObject.activeInHierarchy)
        {
            CurrentUser.Name = UIReferences.References.Login.NameField.text;
        }
        CurrentUser.Vault = new VaultParams();
        CurrentUser.Vault.GoldBars = 0;
        CurrentUser.Vault.Coins = 0;
        CurrentUser.Vault.CoinsPercentsCanTaken = 80;
        CurrentUser.Vault.Assets = new List<string>();
        CurrentUser.Vault.Games = 0;
        CurrentUser.Vault.Boosters = 0;
        CurrentUser.Vault.Shields = 0;
        CurrentUser.IsUnderAttack = "false";
        CurrentUser.TokensList = new List<string>();
        CurrentUser.UserGamesProgress = new UserGamesProgressData();
        CurrentUser.UserGamesProgress.TotalGames = 0;
        CurrentUser.UserGamesProgress.TotalWins = 0;
        CurrentUser.UserGamesProgress.CurrentGames = 0;
        CurrentUser.UserGamesProgress.CurrentWins = 0;
        CurrentUser.XP = 0;
        DatabaseManager.Database.AddUser();
        UIReferences.References.ActiveScreen(ScreensManager.Screens.WELLCOM_BONUSES, ScreensManager.Requires.NONE);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TinySauce.OnGameFinished(0);
            Application.Quit();
        }

        //UIReferences.References.DebugOnScreen("App started with referral data: " + _referId);
    }

    private void CheckAttackTime()
    {
        if (CurrentUser.IsUnderAttack == "true")
        {
            //UIReferences.References.DebugOnScreen(EndAttackTime.ToString());
            if (EndAttackTime != null && EndAttackTime <= DateTime.Now)
            {
                CurrentUser.IsUnderAttack = "false";
                DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.IsUnderAttack), false } });
            }
        }
    }

    public void RemoveRevenge(Revenge revenge)
    {
        CurrentUser.RevengesList.Remove(revenge);
        DatabaseManager.Database.UpdateRevenges();
    }

    public void TestInvite()
    {
        InvitesViewBuilder.Create().Show();
    }
}