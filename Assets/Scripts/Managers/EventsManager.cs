﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Firebase.Auth;

public class EventsManager : MonoBehaviour
{
    public static EventsManager Events;

    private void Awake()
    {
        if (Events == null)
        {
            Events = this;
            return;
        }
        Destroy(gameObject);
    }
}

[Serializable]
public class UnityEventString : UnityEvent<string> { }

[Serializable]
public class UnityEventUser : UnityEvent<User> { }

[Serializable]
public class UnityEventPlayers : UnityEvent<Players> { }

[Serializable]
public class UnityEventImage : UnityEvent<Image> { }

[Serializable]
public class UnityEventBool : UnityEvent<bool> { }

[Serializable]
public class UnityEventInt : UnityEvent<int> { }

[Serializable]
public class UnityEventSprite : UnityEvent<Sprite> { }

[Serializable]
public class UnityEventCellButton : UnityEvent<CellButton> { }

[Serializable]
public class UnityEventFirebaseUser: UnityEvent<FirebaseUser> { }

[Serializable]
public class UnityEventDictionaryStringString : UnityEvent<Dictionary<string, string>> { }

[Serializable]
public class UnityEventDictionaryStringIntFloat : UnityEvent<Dictionary<string, Dictionary<int, float>>> { }

[Serializable]
public class UnityEventDictionaryIntStringInt : UnityEvent<Dictionary<int, Dictionary<string, int>>> { }
[Serializable]
public class UnityEventBuster : UnityEvent<Buster> { }