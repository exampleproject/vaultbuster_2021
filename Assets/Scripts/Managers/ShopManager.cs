﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public Image GoldBarsPanel;
    public Image GamesPanel;
    public Image CoinsPanel;
    public Image BoostersPanel;
    public Image ShieldsPanel;
    public AudioSource ClickSound;
    public IAPManager IAP;

    private Dictionary<string, Dictionary<int, float>> _shopData;

    private void Awake()
    {
        DatabaseManager.Database.GotShop.AddListener(OnGotShopData);
    }

    private void OnGotShopData(Dictionary<string, Dictionary<int, float>> shopData)
    {
        _shopData = shopData;
    }

    public bool IsShopData()
    {
        return _shopData != null;
    }

    public void SetPrices()
    {
        Transform currentContent = null;
        DatabaseManager.UserDataKeys key = DatabaseManager.UserDataKeys.GoldBars;
        foreach (var product in _shopData.Keys)
        {
            switch (product)
            {
                case "gold_bars":
                    currentContent = GoldBarsPanel.transform.Find("Viewport").Find("Content");
                    key = DatabaseManager.UserDataKeys.GoldBars;
                    break;
                case "games":
                    currentContent = GamesPanel.transform.Find("Viewport").Find("Content");
                    key = DatabaseManager.UserDataKeys.Games;
                    break;
                case "coins":
                    currentContent = CoinsPanel.transform.Find("Viewport").Find("Content");
                    key = DatabaseManager.UserDataKeys.Coins;
                    break;
                case "boosters":
                    currentContent = BoostersPanel.transform.Find("Viewport").Find("Content");
                    key = DatabaseManager.UserDataKeys.Boosters;
                    break;
                case "shields":
                    currentContent = ShieldsPanel.transform.Find("Viewport").Find("Content");
                    key = DatabaseManager.UserDataKeys.Shields;
                    break;
            }

            var keys = _shopData[product].Keys.ToList();
            keys.Sort();

            for (int i = 0; i < keys.Count; i++)
            {
                if (currentContent.childCount <= i)
                {
                    Instantiate(currentContent.GetChild(currentContent.childCount - 1), currentContent);
                }

                var sum = currentContent.GetChild(i).Find("Sum").GetComponent<TMPro.TextMeshProUGUI>();
                var sumValue = keys[i];
                sum.text = sumValue.ToString();

                var buyButton = currentContent.GetChild(i).Find("Button");
                var price = buyButton.Find("Text").GetComponent<Text>();
                var priceValue = _shopData[product][sumValue];
                if (product == "gold_bars")
                {
                    price.text = priceValue.ToString("N2");
                    buyButton.GetComponent<Button>().onClick.AddListener(() => IAP.BuyGoldBars(sumValue));
                }
                else
                {
                    price.text = priceValue.ToString();
                    currentContent.GetChild(i).GetComponent<CanvasGroup>().interactable = priceValue <= AppManager.App.CurrentUser.Vault.GoldBars;
                    var shopButton = buyButton.GetComponent<ShopButton>();
                    if (shopButton == null)
                    {
                        shopButton = buyButton.gameObject.AddComponent<ShopButton>();
                    }
                    shopButton.Sum = sumValue;
                    shopButton.Price = (int)priceValue;
                    shopButton.Key = key;
                    shopButton.ClickSound = ClickSound;
                }
            }
        }
    }
}
