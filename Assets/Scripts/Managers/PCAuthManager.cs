﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Firebase;
using Firebase.Extensions;
using Firebase.Auth;
using Google;

public class PCAuthManager : MonoBehaviour
{
    public UnityEventString PCLogedIn;

    private FirebaseAuth _auth;
    private Dictionary<string, FirebaseUser> _userByAuth = new Dictionary<string, FirebaseUser>();
    private bool fetchingToken = false;
    private string _displayName;
    private bool _signInAndFetchProfile = false;

    public static PCAuthManager PCAuthentication;

    private void Awake()
    {
        if (PCAuthentication == null)
        {
            PCAuthentication = this;
            return;
        }
        Destroy(gameObject);
    }

    // Handle initialization of the necessary firebase modules:
    public void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        _auth = FirebaseAuth.DefaultInstance;
        //SignOut();
        _auth.StateChanged += AuthStateChanged;
        _auth.IdTokenChanged += IdTokenChanged;
        //AuthStateChanged(_auth, null);
    }

    // Track state changes of the auth object.
    void AuthStateChanged(object sender, EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        FirebaseUser user = null;
        if (senderAuth != null)
        {
            _userByAuth.TryGetValue(senderAuth.App.Name, out user);
        }
        if (senderAuth == _auth)
        {
            if (senderAuth.CurrentUser != user)
            {
                bool signedIn = user != senderAuth.CurrentUser && senderAuth.CurrentUser != null;
                if (!signedIn && user != null)
                {
                    Debug.Log("Signed out " + user.UserId);
                }
                user = senderAuth.CurrentUser;
                _userByAuth[senderAuth.App.Name] = user;
                if (signedIn)
                {
                    Debug.Log("Signed in " + user.UserId);
                    PCLogedIn.Invoke(user.UserId);
                }
            }
            else
            {
                UIReferences.References.GameFllow.FirstScreen.gameObject.SetActive(true);
            }
        }
    }

    // Track ID token changes.
    void IdTokenChanged(object sender, EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        if (senderAuth == _auth && senderAuth.CurrentUser != null && !fetchingToken)
        {
            senderAuth.CurrentUser.TokenAsync(false).ContinueWithOnMainThread(task =>
            {
                Debug.Log(String.Format("Token[0:8] = {0}", task.Result.Substring(0, 8)));
                //DatabaseManager.Database.SetTokens(task.Result);
            });
        }
    }

    private void OnDestroy()
    {
        if (_auth != null)
        {
            _auth.StateChanged -= AuthStateChanged;
            _auth.IdTokenChanged -= IdTokenChanged;
            _auth = null;
        }
    }

    // Log the result of the specified task, returning true if the task
    // completed successfully, false otherwise.
    private bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string authErrorCode = "";
                FirebaseException firebaseEx = exception as FirebaseException;
                if (firebaseEx != null)
                {
                    authErrorCode = String.Format("AuthError.{0}: ",
                      ((AuthError)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(authErrorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    // Create a user with the email and password.
    public void CreateUserWithEmailAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to create user {0}...", email));

        // This passes the current displayName through to HandleCreateUserAsync
        // so that it can be passed to UpdateUserProfile().  displayName will be
        // reset by AuthStateChanged() when the new user is created and signed in.
        string newDisplayName = _displayName;
        Debug.Log(_auth);
        _auth.CreateUserWithEmailAndPasswordAsync(email, password)
          .ContinueWithOnMainThread((task) =>
          {
              if (LogTaskCompletion(task, "User Creation"))
              {
                  PCLogedIn.Invoke(task.Result.UserId);
                  return UpdateUserProfileAsync(newDisplayName: newDisplayName);
              }
              return task;
          }).Unwrap();
    }

    // Update the user's display name with the currently selected display name.
    public Task UpdateUserProfileAsync(string newDisplayName = null)
    {
        if (_auth.CurrentUser == null)
        {
            Debug.Log("Not signed in, unable to update user profile");
            return Task.FromResult(0);
        }
        _displayName = newDisplayName ?? _displayName;
        Debug.Log("Updating user profile");
        return _auth.CurrentUser.UpdateUserProfileAsync(new UserProfile
        {
            DisplayName = _displayName,
            PhotoUrl = _auth.CurrentUser.PhotoUrl,
        }).ContinueWithOnMainThread(task =>
        {
            if (LogTaskCompletion(task, "User profile"))
            {
                //EventsManager.Events.NewUser.Invoke();
            }
        });
    }

    // Sign-in with an email and password.
    public Task SigninWithEmailAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to sign in as {0}...", email));
        if (_signInAndFetchProfile)
        {
            return _auth.SignInAndRetrieveDataWithCredentialAsync(EmailAuthProvider.GetCredential(email, password)).ContinueWithOnMainThread(HandleSignInWithSignInResult);
        }
        else
        {
            return _auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(HandleSignInWithUser);
        }
    }

    // This is functionally equivalent to the Signin() function.  However, it
    // illustrates the use of Credentials, which can be aquired from many
    // different sources of authentication.
    public Task SigninWithEmailCredentialAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to sign in as {0}...", email));
        if (_signInAndFetchProfile)
        {
            return _auth.SignInAndRetrieveDataWithCredentialAsync(EmailAuthProvider.GetCredential(email, password)).ContinueWithOnMainThread(HandleSignInWithSignInResult);
        }
        else
        {
            return _auth.SignInWithCredentialAsync(EmailAuthProvider.GetCredential(email, password)).ContinueWithOnMainThread(HandleSignInWithUser);
        }
    }

    // Called when a sign-in without fetching profile data completes.
    private void HandleSignInWithUser(Task<FirebaseUser> task)
    {
        if (LogTaskCompletion(task, "Sign-in"))
        {
            Debug.Log(String.Format("{0} signed in", task.Result.DisplayName));
            Debug.Log(task.Result.UserId);
        }
    }

    // Called when a sign-in with profile data completes.
    private void HandleSignInWithSignInResult(Task<SignInResult> task)
    {
        if (LogTaskCompletion(task, "Sign-in"))
        {
            Debug.Log(task.Result.User.UserId);
        }
    }

    private void FacebookButtonClickHandler()
    {
        //ServerAuthManager.Instance.Login(PlatformEnums.Login.Facebook, ProjectSettings.SERVER_TYPE);
        //ServerAuthManager.Instance.Login(PlatformEnums.Login.Facebook, PlatformEnums.Backend.Firebase);
    }

    private void SignOut()
    {
        Debug.Log("Signing out.");
        _auth.SignOut();
    }
}
