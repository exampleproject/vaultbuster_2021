﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Firebase.Functions;

public class FuncitionsManager : MonoBehaviour
{
    public enum DataKeys { GoldBars, Coins, CoinsPercentsCanTaken, Games, Boosters, Shields };

    public static FuncitionsManager Functions;

    public UnityEventPlayers GotBotsOpponents;
    public UnityEventPlayers GotPlayersOpponents;
    public UnityEventPlayers GotFriends;
    public UnityEventPlayers GotRevengesOpponents;

    public int WinCoins { get; private set; }

    private FirebaseFunctions _functions;

    //private Dictionary<DataKeys, string> _keysPath = new Dictionary<DataKeys, string>
    //{
    //    { DataKeys.Coins, "/Vault/Coins" },
    //    { DataKeys.CoinsPercentsCanTaken, "/Vault/CoinsPercentsCanTaken" }
    //};

    private void Awake()
    {
        if (Functions == null)
        {
            Functions = this;
            return;
        }
        Destroy(gameObject);
    }

    public void StartFunctionsManager()
    {
        _functions = FirebaseFunctions.DefaultInstance;
    }

    //public void BotsAttack()
    //{
    //    Debug.Log("BotAttack");
    //    _functions.GetHttpsCallable("BotAttack").CallAsync();
    //}

    public void GetPlayersOpponents(int min, int max)
    {
        var usersFunction = GetUsersFunction(false, min, max);
        usersFunction.ContinueWith(task =>
        {
            Debug.Log(task.Status);
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                Debug.Log(task.Result);
                GotPlayersOpponents.Invoke(task.Result == null ? null : JsonUtility.FromJson<Players>(task.Result));
            }
        });
    }

    public void GetBotsOpponents()
    {
        var usersFunction = GetUsersFunction(true);
        usersFunction.ContinueWith(task =>
        {
            Debug.Log(task.Status);
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                Debug.Log(task.Result);
                GotBotsOpponents.Invoke(task.Result == null ? null : JsonUtility.FromJson<Players>(task.Result));
            }
        });
    }

    private Task<string> GetUsersFunction(bool isBots, int min = 0, int max = 0)
    {
        HttpsCallableReference function;
        var data = new Dictionary<string, object>();
        if (isBots)
        {
            function = _functions.GetHttpsCallable("getBotsByRange");
        }
        else
        {
            if (max != 0 && max != min)
            {
                data["min"] = min;
                data["max"] = max;
            }
            function = _functions.GetHttpsCallable("getPlayersByRange");
        }
        return function.CallAsync(data).ContinueWith(task =>
        {
            Debug.Log(task.Status);
            return (string)task.Result.Data;
        });
    }

    public void SetFriends(string sociallId)
    {
        var setFriendsFunction = SetFriendsFunction(sociallId);
        UIReferences.References.DebugOnScreen(sociallId);
        setFriendsFunction.ContinueWith(task =>
        {
            UIReferences.References.DebugOnScreen(task.Status.ToString());
            Debug.Log(task.Status);
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                Debug.Log(task.Result);
            }
        });
    }

    private Task<string> SetFriendsFunction(string socialId)
    {
        UIReferences.References.DebugOnScreen("SetFriendsFunction");
        HttpsCallableReference function;
        UIReferences.References.DebugOnScreen("1");
        var data = new Dictionary<string, object>();
        UIReferences.References.DebugOnScreen("2");
        data["socialId"] = socialId;
        UIReferences.References.DebugOnScreen("3");
        data["userId"] = AppManager.App.CurrentUser.Id;
        UIReferences.References.DebugOnScreen("4");
        function = _functions.GetHttpsCallable("setFriends");
        UIReferences.References.DebugOnScreen("data");
        return function.CallAsync(data).ContinueWith(task =>
        {
            UIReferences.References.DebugOnScreen("Callback: " + task.Status);
            Debug.Log(task.Status);
            return (string)task.Result.Data;
        });
    }

    public void GetFriends()
    {
        var usersFunction = GetFriendsFunction();
        usersFunction.ContinueWith(task =>
        {
            Debug.Log(task.Status);
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                Debug.Log(task.Result);
                GotFriends.Invoke(task.Result == null ? null : JsonUtility.FromJson<Players>(task.Result));
            }
        });
    }

    private Task<string> GetFriendsFunction()
    {
        HttpsCallableReference function;
        var data = new Dictionary<string, object>();
        data["friendsIds"] = AppManager.App.SocialIds;
        function = _functions.GetHttpsCallable("getFriends");

        return function.CallAsync(data).ContinueWith(task =>
        {
            Debug.Log(task.Status);
            return (string)task.Result.Data;
        });
    }

    public void SendAttackAlarm(string playerId, string senderId, string senderName)
    {
        Debug.Log("send attack");
        var data = new Dictionary<string, object>();
        data["userId"] = playerId;
        data["senderID"] = senderId;
        data["senderName"] = senderName;
        Debug.Log(AppManager.App.CurrentUser.XP);
        data["endTime"] = DateTime.Now.AddSeconds(GameProgressConfig.GameProgress.GetChoosenXPData().TotalGamesTime).ToString(UIReferences.DATE_FORMAT);

        var function = _functions.GetHttpsCallable("sendAttackNotification");
        function.CallAsync(data).ContinueWith((task) =>
        {
            Debug.Log(task.Status);
            //Debug.Log(string.Format("Attacking {0}", playerId));
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        //var message = e.ErrorMessage;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                string result = (string)task.Result.Data;
                Debug.Log(result);
            }
        });
    }

    public void changeCode()
    {
        var data = new Dictionary<string, object>();
        data["userId"] = AppManager.App.AttackBuster.SenderId;

        var function = _functions.GetHttpsCallable("changeCodeMessage");
        function.CallAsync(data).ContinueWith((task) =>
        {
            Debug.Log(task.Status);
            //Debug.Log(string.Format("Attacking {0}", playerId));
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        //var message = e.ErrorMessage;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                string result = (string)task.Result.Data;
                Debug.Log(result);
            }
        });
    }

    public int TransferCoins()
    {
        var coinsToTransfer = 0;

        var usersFunction = TransferCoinsFunction();
        usersFunction.ContinueWith(task =>
        {
            Debug.Log(task.Status);
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                Debug.Log(task.Result);
                WinCoins = coinsToTransfer = int.Parse(task.Result);
                Debug.Log(coinsToTransfer);
            }
        });
        return coinsToTransfer;
    }

    private Task<string> TransferCoinsFunction()
    {
        var data = new Dictionary<string, object>();
        data["userId"] = AppManager.App.CurrentUser.Id;
        data["isBot"] = AppManager.App.OpponentPlayer.IsBot;
        data["opponetId"] = AppManager.App.OpponentPlayer.Uid;
        data["upTo"] = GameProgressConfig.GameProgress.GetChoosenXPData().UpTo;

        var function = _functions.GetHttpsCallable("transferCoins");
        return function.CallAsync(data).ContinueWith(task =>
        {
            Debug.Log(task.Status);
            return (string)task.Result.Data;
        });
    }

    public string UpdateVault(DataKeys key, string dataValue)
    {
        var returnValue = "";
        var updataDataFunction = UpdateVaultFunction(key.ToString(), dataValue);
        updataDataFunction.ContinueWith(task =>
        {
            Debug.Log(task.Status);
            if (task.IsFaulted)
            {
                foreach (var inner in task.Exception.InnerExceptions)
                {
                    if (inner is FunctionsException)
                    {
                        var e = (FunctionsException)inner;
                        // Function error code, will be INTERNAL if the failure
                        // was not handled properly in the function call.
                        var code = e.ErrorCode;
                        Debug.Log(code);
                    }
                }
            }
            else
            {
                Debug.Log(task.Result);
                returnValue = task.Result;
            }
        });

        return returnValue;
    }

    private Task<string> UpdateVaultFunction(string dataKey, string dataValue)
    {
        var data = new Dictionary<string, object>();
        data["text"] = string.Join("|", new string[] { AppManager.App.CurrentUser.Id, dataKey, dataValue });
        //data["dataKey"] = dataKey;
        //data["dataValue"] = dataValue;

        var function = _functions.GetHttpsCallable("updateVault");
        return function.CallAsync(data).ContinueWith(task =>
        {
            Debug.Log(task.Status);
            return (string)task.Result.Data;
        });
    }
}