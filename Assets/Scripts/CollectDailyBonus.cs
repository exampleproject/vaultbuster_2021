﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectDailyBonus : OpenDialog
{
    public AudioSource LobbySound;
    public string WinObject { set; get; }

    public void OnCollectClick()
    {
        var background = UIReferences.References.GameFllow.DailyBonusBackground;
        background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
        switch (WinObject)
        {
            case "gold_bars":
                UIReferences.References.TopBar.WinGoldBars.gameObject.SetActive(true);
                break;
            case "coins":
                UIReferences.References.TopBar.WinCoins.gameObject.SetActive(true);
                break;
            case "games":
                UIReferences.References.TopBar.WinGames.gameObject.SetActive(true);
                break;
            case "boosters":
                UIReferences.References.TopBar.WinBoosters.gameObject.SetActive(true);
                break;
            case "shields":
                UIReferences.References.TopBar.WinShields.gameObject.SetActive(true);
                break;
        }
    }

    private void OnDisable()
    {
        LobbySound.Play();
    }
}
